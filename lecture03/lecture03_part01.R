text <- readLines("pg11000.txt")
# have a look at the first 10 lines
head(text, n = 10)


# use "grep" to find a specific line 
idx <- grep("The life", text)
idx

# use the index number to print that line
text[idx]




# load the package
library(text2map.corpora)
# load the corpora
data("corpus_taylor_swift")
# have a look at the first song's text
corpus_taylor_swift$song_text[1]

download_corpus("corpus_pitchfork")

# load the corpora
data("corpus_pitchfork")
# have a look at the columns
colnames(corpus_pitchfork)

?corpus_pitchfork

library("gutenbergr")

gilgamesh <- gutenberg_download(11000)
# look at row 52-62 of the text column
gilgamesh$text[52:62]



Sys.setenv(NYTIMES_API_KEY = "wfEIxAg5sHVUVYhAK8WpNuUKUWAIuA5G")

library(tidyverse)
library(tidyquant)
# install.packages('quantmod')

# get stock prices for GameStop
gme <- tq_get("GME",
              get = "stock.prices", 
              from = "2020-12-01",
              to = "2021-03-01")






gme |>
ggplot(aes(x = date, y = close)) +
    geom_line() +
    labs(y = "Closing Price (US$)", 
         x = "Date")


library(rtimes)
# install.packages("devtools")
# install_version("rtweet", version = "1.2.1", repos = "http://cran.us.r-project.org")

nyt_arts <- as_search(q = "gamestop",
                      begin_date = "20210115",
                        end_date = "20210215",
                      all_results = TRUE,
                      sleep = 10)





names(nyt_arts)

df_nyt <- nyt_arts$data 

dim(df_nyt)


colnames(df_nyt)

head(df_nyt)

df_nyt$pub_date_posix <- as.POSIXct(df_nyt$pub_date)


# load the rtweet package
library(rtweet)


# group data by 1 day
p_rtimes <- ts_data(df_nyt, "1 day")


# create the base plotting object
p_rtimes <- p_rtimes |>
            ggplot(aes(time, n)) + 
            geom_line()

# now let's clean up the labels and add a title
p_rtimes <- p_rtimes + 
  labs(
  x="Day", 
  y=NULL,
  subtitle="Articles per day",
)

# print to screen
p_rtimes

# let's change the x-axis so it shows every day:
p_rtimes <- p_rtimes +
    scale_x_datetime(date_breaks="1 day", 
                     date_labels="%b-%d")

# x-axis labels are overlapping, 
# so let's rotate them a bit
p_rtimes <- p_rtimes + 
    theme(axis.text.x = element_text(angle=45, 
                                     hjust=.65, 
                                     vjust=.5))
# print to screen
p_rtimes

p_data <- df_nyt |>
        group_by(news_desk) |>
        summarize(n = n()) |>
        mutate(news_desk_sorted = fct_reorder(news_desk, n))

head(p_data)


p_nyt1 <- p_data |> 
          ggplot(aes(x=news_desk_sorted, y=n)) +
            geom_point() +
            coord_flip() +
            labs(y="Frequency", 
                 x=NULL)

# print to screen
p_nyt1

