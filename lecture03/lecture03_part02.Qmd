---
title: "Acquiring Texts"
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

```

## Acquiring Text

### Setup
- Open an RStudio session
- Follow along the lecture script: `lecture03_part02.R`

## Acquiring Text

- Scraping with dedicated APIs
  - `rtimes` packages is a wrapper for the _NYTs_ API
  - We need often need "keys" to authenticate us
  
## Acquiring Text

- Scraping with dedicated APIs
  - Site might not have an API for developers
  - There are no ready-made R packages
  - API may be too limiting

## Acquiring Text

### Custom Scraping
- Larger learning curve
- Scrapers have to be tailored for each site
- Small changes in the site can break scraper


## Acquiring Text

### Custom Scraping
- Look at the Amazon Reviews for the movie _Naked Lunch_
    - https://www.amazon.com/gp/product/B01N76CIC8
    
## Acquiring Text

Just like we used `readLines()` to read in plain text, we can read in raw HTML from a url:

\footnotesize
```{r}
#| echo: TRUE
#| eval: FALSE
#| message: FALSE


page <- readLines("https://www.amazon.com/gp/product/B01N76CIC8")

# this will tell us how many lines of HTML we have:
length(page)
```

    [1] 135

## Acquiring Text

\small
```{r}
#| echo: TRUE
#| eval: FALSE

head(page)
```


    [1] "<!DOCTYPE html>"                                                                                  
    [2] "<!--[if lt IE 7]> <html lang=\"en-us\" class=\"a-no-js a-lt-ie9 a-lt-ie8 a-lt-ie7\"> <![endif]-->"
    [3] "<!--[if IE 7]>    <html lang=\"en-us\" class=\"a-no-js a-lt-ie9 a-lt-ie8\"> <![endif]-->"         
    [4] "<!--[if IE 8]>    <html lang=\"en-us\" class=\"a-no-js a-lt-ie9\"> <![endif]-->"                  
    [5] "<!--[if gt IE 8]><!-->"                                                                           
    [6] "<html class=\"a-no-js\" lang=\"en-us\"><!--<![endif]--><head>"    

## Acquiring Text

### Custom Scraping

- The `rvest` package to help us "harvest" a webpage
    - "Parses" HTML files from a URL
    - extract the relevant **HTML nodes**
    - convert them into a data frame

## Acquiring Text

### Custom Scraping
- Any patterns can be automated 
    - The URLs follow a pattern
    - We'll use `paste0()` to piece together URLs
    - We don't need to know each page's URL
    
    
## Acquiring Text

\footnotesize

```{r}
#| echo: TRUE
#| eval: TRUE


url <- paste0(
  "https://arxiv.org/search/advanced?advanced=&",
  "terms-0-operator=AND&terms-0-term=&",
  "terms-0-field=title&",
  "classification-physics_archives=all&",
  "classification-statistics=y&",
  "classification-include_cross_list=include&",
  "date-filter_by=all_dates&date-year=&",
  "date-from_date=&date-to_date=&",
  "date-date_type=submitted_date&",
  "abstracts=show&size=50&",
  "order=-announced_date_first"
)

```


## Acquiring Text

### Custom Scraping
- `rvest`
    - `read_html()` function
    - a bit like `readLines()`


## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: TRUE


library(tidyverse)
library(rvest)
```
```{r}
#| echo: TRUE
#| eval: FALSE

# chaining
url |> read_html()
```
```{r}
#| echo: TRUE
#| eval: TRUE
# nesting
read_html(url)

```


## Acquiring Text

### Custom Scraping

- We can identify HTML nodes using two kinds of IDs:
    - CSS (Cascading Style Sheets) 
    - XPath code
- We’ll focus on using CSS

## Acquiring Text

### Custom Scraping
- Most browsers will show the raw HTML:
  - Firefox and Chrome: 
    - `ctrl + shift + i` (Windows/Linux)
    - `command + option + i` (Mac)
- We use the patterns in HTML to create structured datasets
  - We want to find "ids" for different sections of the page
  
  
## Acquiring Text

### Custom Scraping
- Helper tools:
  - [SelectorHub](https://selectorshub.com/selectorshub/)
  - [**SelectorGadget**](https://selectorgadget.com/) <-
  
  
## Acquiring Text

Click and hold this link

![](../images/img_selector_gadget_bookmark.png)

Drag it to your bookmarks bar

![](../images/img_selector_gadget_bookmark_bar.png)

## Acquiring Text
### Custom Scraping
- Go to the [ArXiv url](https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=&terms-0-field=title&classification-physics_archives=all&classification-statistics=y&classification-include_cross_list=include&date-filter_by=all_dates&date-year=&date-from_date=&date-to_date=&date-date_type=submitted_date&abstracts=show&size=50&order=-announced_date_first) and then click your **SelectorGadget** bookmark
  - the IDs will be in the gray bar at the bottom
  - orange: shows the invisible structure as you move around
  - green: click on content you want
  - yellow: all the content included with that node 
  - red: any content you "deselect"


## Acquiring Text

![Selecting the top result gives us ".arxiv-result" as the path](../images/img_selector_gadget_arxiv.png)

## Acquiring Text

![Clicking around we land on ".abstract-full" for only the abstract text](../images/img_selector_gadget_arxiv_abstract.png)


## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: TRUE


arxiv_element <- url |> 
            read_html() |> 
            html_elements(".abstract-full") 

head(arxiv_element)
```

## Acquiring Text

Use `html_text()` or `html_text2()` to strip the HTML markup characters:
\footnotesize
```{r}
#| echo: TRUE
#| eval: TRUE


# this will output a list
arxiv_text <- url |> 
            read_html() |> 
            html_elements(".abstract-full") |>
            html_text()

# Let's check our catch!
arxiv_text[1]

```


## Acquiring Text

**How do we get all the abstracts?**

\footnotesize
```{r}
#| echo: TRUE
#| eval: TRUE

n_papers <- 106596
per_page <- 50

# we'll round that up with ceiling
n_pages <- ceiling(n_papers/per_page)
n_pages
```

Over 2000 pages is *way too much* for our demonstration, so let's just do 15 pages or 750 papers.
```{r}
#| echo: TRUE
#| eval: TRUE

15 * 50
```

## Acquiring Text

We need the starting paper number for each page.
To do that, well create a list of numbers from 0 and 700 by 50:
```{r}
#| echo: TRUE
#| eval: TRUE
paper_number <- seq(0, 700, by = 50)
# have a look
head(paper_number)
```


## Acquiring Text

Let's create 12 new urls by "pasting" together our base url with our number list!

```{r}
#| echo: TRUE
#| eval: TRUE


url_list <- paste0(url, "&start=", paper_number)

# Check that it's the right length
length(url_list)
  
```
 
## Acquiring Text
### Control Processes
- Loops 
- Apply

## Acquiring Text
### Control Processes
- Loops 
  - Tell `R` to do something over and over again
  - One of the best reasons to learn how to code

    
## Acquiring Text
### Control Processes
- `lapply()`
  - takes a list of elements
  - "applies" a function to each element


## Acquiring Text

For example:
```{r}
#| echo: TRUE
#| eval: TRUE


my_list <- c(1,2,3)

lapply(my_list, function(i){ i*2 })

```

## Acquiring Text

We'll first "create a session" to mimic an actual browser
```{r}
#| echo: TRUE
#| eval: FALSE

uast <- "Mozilla/5.0 (X11; Linux x86_64)
         AppleWebKit/537.36 (KHTML, like Gecko)
         Chrome/127.0.0.0 Safari/537.36"
sess <- session(domain, httr::user_agent(uast))
```


## Acquiring Text
```{r}
#| echo: TRUE
#| eval: FALSE

# download the entire html for all 12 pages
page_list <- lapply(url_list,
              function(page_url, sess) {
                 RES <- page_url |> 
                        session_jump_to(x = sess) |> 
                        read_html()
                 return(RES)
            }, sess
          )

length(page_list)
```
```
[1] 15
```

## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: FALSE

paper_abstract <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".abstract-full") |>
            html_text()
    }
)

```


## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: FALSE

paper_title <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".is-5") |>
            html_text2() # removes excess whitespace
    }
)



```

## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: FALSE

paper_authors <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".authors") |>
            html_text()
    }
)
```

## Acquiring Text

```{r}
#| echo: TRUE
#| eval: FALSE

paper_subj <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".tags.is-inline-block") |>
            html_text2() # removes excess whitespace
    }
)
```


## Acquiring Text

```{r}
#| echo: TRUE
#| eval: FALSE

paper_date <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".mathjax+ .is-size-7") |>
            html_text2() # removes excess whitespace
    }
)

```

## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: FALSE

paper_id <- lapply(
    page_list,
    function(page) {
        page |>
            html_elements(".is-inline-block > a") |>
            html_text()
    }
)

```

## Acquiring Text

\footnotesize
```{r}
#| echo: TRUE
#| eval: FALSE

# this "binds" all our lists together into a dataframe
df_arxiv <- data.frame(
    id = unlist(paper_id),
    authors = unlist(paper_authors),
    title = unlist(paper_title),
    abstract = unlist(paper_abstract),
    tags = unlist(paper_subj),
    paper_date = unlist(paper_date),
    scrape_date = Sys.Date()
)


```
```{r}
#| echo: FALSE
#| eval: FALSE
#| message: FALSE
#| purl: FALSE
saveRDS(df_arxiv, "../data/arxiv_scrape.Rds")
```
```{r}
#| echo: FALSE
#| eval: TRUE
#| message: FALSE
#| purl: FALSE
df_arxiv <- readRDS("../data/arxiv_scrape.Rds")
```
Check the dimensions of the data.frame:
```{r}
#| echo: TRUE
#| eval: TRUE

dim(df_arxiv)
```


## Acquiring Text

Let's clean up the tags a bit:
```{r}
#| echo: TRUE
#| eval: TRUE

# what is the most tags a paper has?
maximum <- max(str_count(df_arxiv$tags, "\\S+"))

maximum
```

## Acquiring Text

Now we know how many columns we need. 
We can separate each tag per paper into a column using `separate()`:^[This code is inspired by [stackoverflow.com/a/56356703](https://stackoverflow.com/a/56356703).]
```{r}
#| echo: TRUE
#| eval: TRUE

df_arxiv <- df_arxiv |>
    separate(tags, paste0("tag_", seq_len(maximum)),
             sep = "\\s+", extra = "merge", fill = "right")

```


## Acquiring Text

We also need to wrangle the `paper_date` column.

```{r}
#| echo: TRUE
#| eval: TRUE

df_arxiv$paper_date[1]
```


## Acquiring Text

We'll use `gsub()` and some regex to extract a substring
```{r}
#| echo: TRUE
#| eval: TRUE

# this is a "regular expression"
regex_pattern <- "^.*Submitted\\s*|\\s*;.*$"

df_arxiv <- df_arxiv |>
   mutate(submit_date = gsub(regex_pattern, "", paper_date))

# check our work
df_arxiv$submit_date[1]
```

Then we'll use `parse_date_time()` to tell `R` this is a date:
```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE

library(lubridate)

df_arxiv <- df_arxiv |>
   mutate(date_new = parse_date_time(submit_date, orders = c("dmy")))

```

## Acquiring Text

We will "melt" or "pivot" our `df_arxiv` dataframe so each row is a date by tag.

```{r}
#| echo: TRUE
#| eval: TRUE

melt_arxiv <- df_arxiv |>
    select(date_new, tag_1:tag_6) |>
    pivot_longer(!date_new, values_drop_na = TRUE)

```

## Acquiring Text

Finally, we'll include only "statistics" tags with `filter()`
We'll rename them with `recode()`
Then, we'll count how many tag occurs per day
```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE

melt_arxiv <- melt_arxiv |>
    filter(grepl("stat.", value, fixed = TRUE)) |>
    mutate(stats = recode(value,
        "stat.AP" = "Applications",
        "stat.CO" = "Computation",
        "stat.ME" = "Methodology",
        "stat.ML" = "Machine Learning",
        "stat.OT" = "Other Statistics")) |>
    group_by(stats, date_new) |>
    summarise(count = n())

```


## Acquiring Text

Using `ggplot()`, we'll divide our data into separate plots using `facet_wrap()` and fit a line that summarizes the data.^[Using smoothed conditional means.]

```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE
#| output-location: slide
#| out.width: "85%"
#| fig.cap: "Papers by Category Over Time"

melt_arxiv |>
  ggplot(aes(x = date_new, y = count)) +
  geom_smooth() +
  labs(x = "Date", y = "Average",
      subtitle = "Papers per Stats Category") +
  facet_wrap(~stats, nrow = 1)

```



## References {.allowframebreaks}