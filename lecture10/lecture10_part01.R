library(tidyverse)
library(tidymodels)
library(text2map)
library(text2map.corpora)
library(glmnet)
library(rsample)
library(text2vec)

shelley <- data.frame(
      text = c("listen to me frankenstein",
             "you accuse me of murder",
             "and yet you would",
             "with a satisfied conscience",
             "destroy your own creature")
      )
butler <- data.frame(
     text = c("we are earthlife preparing to",
            "take root in new ground",
            "earthlife fulfilling its purpose",
            "its promise its destiny")
     )

shelley$butler <- 0
butler$butler <- 1
corpus <- rbind(shelley, butler)

dtm <- corpus |> 
       rowid_to_column(var = "doc_id") |>
       dtm_builder(text = text, 
                   doc_id = doc_id)

dtm[1:2, 1:4]

dtm <- dtm |>
       as.matrix() |> 
       as.data.frame()

dtm[1:2, 1:4]

dtm$butler <- corpus$butler


mod <- glm(butler ~ ., 
           family = binomial, 
           data = dtm)



tidy(mod) |> 
    select(term, estimate) |>
    slice_max(estimate, n = 4)

tidy(mod) |> 
    select(term, estimate) |>
    slice_min(estimate, n = 4)


download_corpus("corpus_senti_bench")

data("corpus_senti_bench")

df_sb <- corpus_senti_bench |>
  mutate(text = tolower(text), 
         text = gsub("[[:punct:]]+", " ", text),
         text = gsub("[[:digit:]]+", " ", text),
         text = str_squish(text)) |>
  mutate(positive = ifelse(polarity > 0, TRUE, FALSE))


init <- initial_split(df_sb,
                      prop = 3/4,
                      strata = "source")

df_trn <- training(init)
df_tst <- testing(init)


dtm_trn <- df_trn |>
           dtm_builder(text = text, 
                       doc_id = doc_id) |>
           dtm_stopper(stop_docfreq = c(15, 8700))

dtm_tst <- df_tst |>
           dtm_builder(text = text, 
                       doc_id = doc_id,
                       vocab = colnames(dtm_trn))

dtm_trn[1:3, 1:5]

dtm_tst[1:3, 1:5]


dtm_trn <- normalize(dtm_trn)
dtm_tst <- normalize(dtm_tst)



fit <-  cv.glmnet(x = dtm_trn,
                  y = df_trn$positive,
                  family = "binomial", 
                  intercept = FALSE)



confusion.glmnet(fit, 
                 newx = dtm_tst, 
                 newy = df_tst$positive,
                 s = "lambda.min")

