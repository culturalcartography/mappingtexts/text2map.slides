---
title: "Extended Deductive: Part 2"
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

```
 


## Extended Deductive: Document Classifiers

### Setup
- Open an RStudio session
- Follow along the lecture script: `lecture10_part02a.R`


## Extended Deductive: Document Classifiers

- Amazon Fine Foods Reviews
    - people rate whether they found the review "helpful"
    - let's use it to classify a review as "helpful" or not
 
 
## Extended Deductive: Document Classifiers

```{r}
#| eval: TRUE
#| echo: TRUE
#| warning: FALSE
#| message: FALSE

library(text2map)
library(tidyverse)
library(rsample)
library(caret)
library(quanteda.textmodels)
library(quanteda)


```


## Extended Deductive: Document Classifiers

```{r}
#| eval: TRUE
#| echo: TRUE
data("corpus_finefoods10k", package = "text2map.corpora")

df_ff <- corpus_finefoods10k |> 
      mutate(text = tolower(text), 
             text = gsub("[[:punct:]]+", " ", text),
             text = gsub("[[:digit:]]+", " ", text),
             text = str_squish(text))

# to further speed this up, from this random sample of 10k,
# get a random sample of 2k docs
set.seed(18015)
# df_ff <- sample_n(df_ff, 2000)
dim(df_ff)

```

## Extended Deductive: Document Classifiers {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE

df_ff <- df_ff |> 
  mutate(
    helpful = ifelse(helpfulness_numerator > 0, "helpful", "unhelpful"),
    helpful = factor(helpful, levels = c("helpful", "unhelpful"))
  )

# only two levels?
summary(df_ff$helpful)

```



## Extended Deductive: Document Classifiers


```{r}
#| eval: TRUE
#| echo: TRUE
init <- initial_split(df_ff,
                      prop = 3/4,
                      strata = "helpful")

df_trn <- training(init)
df_tst <- testing(init)
     
```

## Extended Deductive: Document Classifiers {.smaller}

Remember, the vocab has to match exactly
```{r}
#| eval: TRUE
#| echo: TRUE
dtm_trn <- df_trn |>
           dtm_builder(text = text, 
                       doc_id = review_id) |> 
           dtm_stopper(stop_docfreq = c(3L, Inf))

dtm_tst <- df_tst |>
           dtm_builder(text = text, 
                       doc_id = review_id,
                       vocab = colnames(dtm_trn))

```

## Extended Deductive: Document Classifiers

This is what we used last class (don't run):
```{r}
#| eval: FALSE
#| echo: TRUE
#| purl: FALSE

lr_fit <-  cv.glmnet(x = dtm_trn,
                     y = df_trn$helpful,
                     family="binomial", 
                     intercept = FALSE)

```

This is just a classifier using logistic regression ("binomial")

## Extended Deductive: Document Classifiers


- `quanteda.textmodels`
  - `textmodel_lr`: logistic regression (a.k.a. maximum entropy) 
  - `textmodel_nb`: naive Bayes (a.k.a. simple Bayes)
  - `textmodel_svm`: support-vector machine


## Extended Deductive: Document Classifiers

Fit a logistic regression:
```{r}
#| eval: TRUE
#| echo: TRUE
lr_fit <- textmodel_lr(x = as.dfm(dtm_trn), 
                       y = df_trn$helpful)

```
Predict the labels of test set:

```{r}
#| eval: TRUE
#| echo: TRUE
lr_pred <- predict(lr_fit, 
                   newdata = as.dfm(dtm_tst))
```

## Extended Deductive: Document Classifiers

- How well did we do? 
  - We want to minimize errors:
    - False positives
    - False negatives

## Extended Deductive: Document Classifiers

- How well did we do? 
  - *Precision*: correct positive predictions relative to total positive predictions
  - *Recall*: correct positive predictions relative to total actual positives

## Extended Deductive: Document Classifiers

- How well did we do? 
  - *F1* of 1 means we correctly classified all cases
  - *F1* of 0 means we correctly classified no cases

## Extended Deductive: Document Classifiers {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE
# how well did we do?
confusionMatrix(lr_pred, df_tst$helpful, mode = "prec_recall")

```


## Extended Deductive: Document Classifiers

Fit a naive Bayes:
```{r}
#| eval: TRUE
#| echo: TRUE
nb_fit <- textmodel_nb(x = as.dfm(dtm_trn), 
                       y = df_trn$helpful)
```
Predict the labels of test set:
```{r}
#| eval: TRUE
#| echo: TRUE
nb_pred <- predict(nb_fit, 
                   newdata = as.dfm(dtm_tst))
```

## Extended Deductive: Document Classifiers {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE
#| # how well did we do?
confusionMatrix(nb_pred, df_tst$helpful, mode = "prec_recall")
```

## Extended Deductive: Document Classifiers

```{r}
#| eval: TRUE
#| echo: TRUE

# using Support-Vector Machine
sv_fit <- textmodel_svm(x = as.dfm(dtm_trn), 
                        y = df_trn$helpful)
```
Again, predict the labels of test set:
```{r}
#| eval: TRUE
#| echo: TRUE
sv_pred <- predict(sv_fit, 
                   newdata = as.dfm(dtm_tst))
```

## Extended Deductive: Document Classifiers  {.smaller}


```{r}
#| eval: TRUE
#| echo: TRUE
confusionMatrix(sv_pred, df_tst$helpful, mode = "prec_recall")

```

 
## Extended Deductive: Document Classifiers

- Neural Network Classifiers
  - "layers" or "steps"
  - Technically, it must have at least one "hidden" layer


## Extended Deductive: Document Classifiers

```{r}
#| eval: TRUE
#| echo: TRUE
#| message: FALSE
library(keras3)
library(tensorflow)

ke_dtm_trn <- array_reshape(as.matrix(dtm_trn), dim = dim(dtm_trn))
ke_dtm_tst <- array_reshape(as.matrix(dtm_tst), dim = dim(dtm_tst))

y_trn <- to_categorical(df_trn$helpful == "helpful", 2)
y_tst <- to_categorical(df_tst$helpful == "helpful", 2)

```

This just reshapes our matrices into a form the following functions prefer.^[Note the `y_trn` and the `y_tst` matrices now have two binary columns. The first column the corresponds to "unhelpful" and the second column is "helpful."]

## Extended Deductive: Document Classifiers


- Neural Net Classifier
  - **Model architecture** includes the number and types of "layers" and how we are going to measure accuracy and update our weights as we go along

## Extended Deductive: Document Classifiers


This will *prepare* our model (but won't run it)

```{r}
#| eval: TRUE
#| echo: TRUE
#| warning: FALSE
nn_model <- keras_model_sequential()

nn_model |>
  layer_dense(units = 128, activation = "relu") |>
  layer_dense(units = 2, activation = 'sigmoid')

```

## Extended Deductive: Document Classifiers


- Our neural net has three layers:
  - Input DTM with `r ncol(dtm_trn)` "dimensions"
  - The "hidden" layer reduces our `r ncol(dtm_trn)` to 128 dimensions
  - Output layer, reducing the 128 down to just 2: our outcome
- Without the hidden layer, this is literally *logistic regression*!

## Extended Deductive: Document Classifiers {.smaller}

Configure our loss, optimizer, and accuracy measures
```{r}
#| eval: TRUE
#| echo: TRUE
nn_model |> 
  compile(
    loss = "binary_crossentropy",
    optimizer = "rmsprop",
    metrics = "accuracy"
  )

```
- **loss**: how we calculate the difference between true value and predicted values
- **optimizer**: how the model is updated based on loss



## Extended Deductive: Document Classifiers

Now we can "fit" or train or model -- let's go through just 10 rounds
```{r}
#| eval: TRUE
#| echo: TRUE
#| message: FALSE

history <- nn_model |> 
        fit(ke_dtm_trn, y_trn, epochs = 10)

```



## Extended Deductive: Document Classifiers

```{r}
#| eval: TRUE
#| echo: FALSE
#| message: FALSE
plot(history)

```


## Extended Deductive: Document Classifiers

We can look at the architecture we just constructed:
```{r}
#| eval: TRUE
#| echo: TRUE
summary(nn_model, line_length = 60L)

```

## Extended Deductive: Document Classifiers

- Accuracy
  - How many errors?
- Loss
  - How big are the errors?

## Extended Deductive: Document Classifiers


|          | High Accuracy    | Low Accuracy     |
| --------:|:----------------:|:----------------:|
| **Low Loss** |a few small errors|many small errors |
| **High Loss**|a few huge errors |many huge errors  |

: Classifier Performance {.striped}



## Extended Deductive: Document Classifiers

- Accuracy
  - How many errors?
- Loss
  - How big are the errors?

## Extended Deductive: Document Classifiers

Check how we did on our the training set:
```{r}
#| eval: TRUE
#| echo: TRUE
nn_model |> 
  evaluate(ke_dtm_trn, y_trn, verbose = 0)

```

Almost perfect.. on the training set...

## Extended Deductive: Document Classifiers {.smaller}

Let's predict the test set and create a confusion matrix:
```{r}
#| eval: TRUE
#| echo: TRUE
nn_pred <- nn_model |> 
  predict(ke_dtm_tst, verbose = 0) |> 
  as.data.frame() |>
  mutate(pred = ifelse(round(V1) == 0, "helpful", "unhelpful"), 
         pred = factor(pred, levels = c("helpful", "unhelpful")))
  

confusionMatrix(nn_pred$pred, df_tst$helpful, mode = "prec_recall")

```


## Extended Deductive: Document Classifiers

- Using word embeddings within the neural network
  - We are already generating an "embedding" representation of the documents -- but what about the words?
  - Let's create word embeddings as an intermediary layer in our neural network.

## Extended Deductive: Document Classifiers

- We are going to use a **document-sequence matrix**
  - First we associate each word in the vocabulary with a unique integer
  - Then select the first $N$ most frequent words in the corpus

## Extended Deductive: Document Classifiers

- In the **document-sequence matrix**:
  - the columns are the *position* of a word
  - the cell is the integer associated with the word

## Extended Deductive: Document Classifiers

```{r}
#| eval: TRUE
#| echo: TRUE
n_words <- max(str_count(df_ff$text))

seq_trn <- df_trn |>
    seq_builder(text, maxlen = n_words)

seq_tst <- df_tst |>
    seq_builder(text, maxlen = n_words)

n_vocab <- length(attr(seq_trn, "dic"))
```

## Extended Deductive: Document Classifiers {.smaller}


```{r}
#| eval: TRUE
#| echo: TRUE
#| warning: FALSE
ke_mod <- keras_model_sequential()

ke_mod <- ke_mod |>
  layer_embedding(input_dim = n_vocab + 1, # add 1 for "[UNK]"
                  output_dim = 300, 
                  mask_zero = TRUE) |> # zero is for padding
  layer_global_average_pooling_1d() |>
  layer_dense(units = 128, activation = "relu") |>
  layer_dense(units = 2, activation = 'sigmoid')
```


## Extended Deductive: Document Classifiers

- Using word embeddings within the neural network
  - The input to the next hidden layer won't be the right "shape"
  - So, we include a layer that averages all the vectors in each document
  - This vector representation of the document is passed to the same layers as in our first model


## Extended Deductive: Document Classifiers

> Our loss, optimizer, and accuracy metric are the same as before. The only thing to note is that we use our *document-sequence matrix* to predict our outcome labels.

```{r}
#| eval: TRUE
#| echo: TRUE
#| warning: FALSE
ke_mod <- ke_mod |>
compile(
  loss = "binary_crossentropy",
  optimizer = "rmsprop",
  metrics = "accuracy"
)


history <- ke_mod |>
  fit(seq_trn, y_trn, epochs = 10)
```


## Extended Deductive: Document Classifiers {.smaller}

We can look at the architecture we just constructed:
```{r}
#| eval: TRUE
#| echo: TRUE
summary(ke_mod, line_length = 60L)

```


## Extended Deductive: Document Classifiers {.smaller}

Let's predict the test set and create a confusion matrix:
```{r}
#| eval: TRUE
#| echo: TRUE

nn_pred1 <- ke_mod |> 
  predict(seq_tst, verbose = 0) |>
  as.data.frame() |>
  mutate(pred = ifelse(round(V1) == 0, "helpful", "unhelpful"), 
         pred = factor(pred, levels = c("helpful", "unhelpful")))
  

confusionMatrix(nn_pred1$pred, df_tst$helpful, mode = "prec_recall")


```


## Extended Deductive: Document Classifiers {.smaller}

We can also extract the word embeddings we trained up while fitting the model:
```{r}
#| eval: TRUE
#| echo: TRUE
embeddings <- get_weights(ke_mod)[[1]]

# add rownames from our dictionary
# Keras throws in an "unknown" token
rownames(embeddings) <- c("[UNK]", attr(seq_trn, "dic"))
```

Note: When training embeddings, Keras will add a token for "unknown"

## Extended Deductive: Document Classifiers {.smaller}

We can use them like any other embeddings.
```{r}
#| eval: TRUE
#| echo: TRUE

# let's see how often some words co-occur
sum(grepl("duck", df_ff$text) & grepl("coffee", df_ff$text))
sum(grepl("caffeine", df_ff$text) & grepl("coffee", df_ff$text))
sum(grepl("green", df_ff$text) & grepl("tea", df_ff$text))
sum(grepl("drink", df_ff$text) & grepl("coffee", df_ff$text))
sum(grepl("drink", df_ff$text) & grepl("tea", df_ff$text))
```


## Extended Deductive: Document Classifiers {.smaller}

What are their similarities in the embeddings?
```{r}
#| eval: TRUE
#| echo: TRUE

words <- c("duck", "drink", "caffeine", "green", "tea", "coffee")
sims <- text2vec::sim2(embeddings[words, ], method = "cosine")

sims["duck", "coffee"]
sims["caffeine", "coffee"]
sims["green", "tea"]
sims["drink", "coffee"]
sims["drink", "tea"]

```


## References {.allowframebreaks}