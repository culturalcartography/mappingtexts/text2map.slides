---
title: "Wrangling Words: Part 1"
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

```

## Wrangling Words

### Setup

-   Open an RStudio session
-   Follow along the lecture script: `lecture05_part01.R`

## Wrangling Words

-   Two wrangling levels:
    -   Character-level
        -   ú --\> u
        -   Raúl --\> Raul
    -   Word-level
        -   Gone, Went, Goes --\> Go
        -   Judgement --\> Judgment

## Wrangling Words

-   Two wrangling strategies:
    -   Replacing (or Merging)
    -   Removing

## Wrangling Words {.smaller}

|                 | Removing | Replacing/Merging |
|:----------------|:--------:|:-----------------:|
| Transliterating |  FALSE   |       TRUE        |
| Punctuation     |   TRUE   |       TRUE        |
| Numbers         |   TRUE   |       TRUE        |
| Whitespace      |   TRUE   |       TRUE        |
| Capitalization  |  FALSE   |       TRUE        |

: Text Wrangling Procedures at the Character-Level

## Wrangling Words {.smaller}

|                      | Removing | Replacing/Merging |
|:---------------------|:--------:|:-----------------:|
| Kerning              |  FALSE   |       TRUE        |
| Elongation           |  FALSE   |       TRUE        |
| Emojis/Emoticons     |   TRUE   |       TRUE        |
| Contractions         |  FALSE   |       TRUE        |
| "Misspelling"        |  FALSE   |       TRUE        |
| Stemming/Lemmatizing |  FALSE   |       TRUE        |
| Grammatical Stopping |   TRUE   |       FALSE       |
| Frequency Stopping   |   TRUE   |       FALSE       |
| Stoplists            |   TRUE   |       FALSE       |

: Text Wrangling Procedures at the Word-Level

## Wrangling Words

```{r}
#| eval: FALSE
#| echo: TRUE
# look at the documentation for the textclean package
help(package = "textclean")
```

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE

# load corpus from text2map.corpora
data(corpus_cmu_blogs100, package = "text2map.corpora")

# rename to save some typing
df_cmu <- corpus_cmu_blogs100

dim(df_cmu)
colnames(df_cmu)
df_cmu$rating[1:4]
```

## Wrangling Words

Make a DTM with the original text:

```{r}
#| eval: TRUE
#| echo: TRUE
library(text2map) 
library(tidyverse)

dtm <- df_cmu |> 
       dtm_builder(text = text, 
                   doc_id = doc_id)

dim(dtm)
```

## Wrangling Words

Lowercase our text, and save it as a new variable

```{r}
#| eval: TRUE
#| echo: TRUE
df_cmu <- df_cmu |> 
      mutate(text_lower = tolower(text))

# create a new DTM
dtm_lower <- df_cmu |> 
             dtm_builder(text = text_lower, 
                         doc_id = doc_id)

dim(dtm_lower)
```

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE
# compare number of columns
ncol(dtm) - ncol(dtm_lower)
```

-   Lowercasing
    -   Reduced our DTM by `r ncol(dtm) - ncol(dtm_lower)` columns!
    -   This limits our "feature space"
    -   But, we lose whatever information capitals convey
    -   **Always a trade-off**

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE
df_cmu <- df_cmu |> 
    mutate(text_punc = gsub("[[:punct:]]", " ", text))


dtm_punc <- df_cmu |> 
             dtm_builder(text = text_punc, 
                         doc_id = doc_id)

dim(dtm_punc)
```

## Wrangling Words

### Regular Expression (Regex)

-   A *pattern* of characters that matches sequences (or strings) of characters
-   Originally developed for Unix, so most systems use it


## Wrangling Words

### Regular Expression (Regex)

-   A *pattern* of characters that matches sequences (or strings) of characters
-   Originally developed for Unix, so most systems use it
-   _**I DO NOT EXPECT YOU TO MEMORIZE REGEX PATTERNS**_

## Wrangling Words

### Regular Expression (Regex)

-   Search for a string of characters
    -   Literal or fixed characters
    -   Special or variable characters

## Wrangling Words

### Regular Expression (Regex)

-   Search with `ctrl + f`/`cmd + f` or in a Search Engine
    -   We can match exact, *literal* characters
    -   We can use various *fuzzy* matches

## Wrangling Words

![Regex search options in Google Docs](../images/img_regex_gdocs.png){width=60%}

## Wrangling Words

### Regular Expression (Regex)

-   Example:
    -   Say, we want to find every text with "fine"
    -   We also want "fiiiiine" or "fiiine" etc..
    -   We could write out every variation:
        -   fine
        -   fiine
        -   fiiine
        -   fiiiine
        -   To infinity...

## Wrangling Words

![Sample Regex Pattern](../images/img_regex_fine.png)

## Wrangling Words

### Regular Expression (Regex){.smaller}

-   Regex patterns can get pretty complicated
    - e.g., this matches 99.99% of all email addresses:
        - `\b[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+.[a-zA-Z0-9.-]+\b`

## Wrangling Words{.smaller}

|Character | Matches                       |
| --------:|:------------------------------|
|`\b`      | Word boundary                 |
|`[`       | Open literal characters       |
|`a-z`     | Lowercase English letters     |
|`A-Z`     | Uppercase English letters     |
|`0-9`     | Any number                    |
|`.`       | Literal period                |
|`-`       | Literal dash                  |
|`]`       | Close literal characters      |
|`+`       | Repeat previous one or more times|
Table: Breakdown of `\b[a-zA-Z0-9.-]+`


## Wrangling Words

### Regular Expression (Regex)

-   A *pattern* of characters that matches sequences (or strings) of characters
-   _**DO NOT EXPECT TO MEMORIZE REGEX PATTERNS**_


## Wrangling Words

### Regular Expression (Regex)

- `grep`
    - **G**lobally search for a **R**egular **E**xpression and **P**rint matching lines
    - Originally developed for Unix, so most systems use it
    
## Wrangling Words

### Regular Expression (Regex)

- `grep`
    - **G**lobally search for a **R**egular **E**xpression and **P**rint matching lines
    - Originally developed for Unix, so most systems use it
    - We can match literal characters or regex patterns

## Wrangling Words

\footnotesize

```{r}
#| eval: TRUE
#| echo: TRUE

# create a vector of character "strings"
texts <- c(
          "this is fine",
          "this is fiiiine",
          "never gonna give you up",
          "it's not fine",
          "no need to define",
          "don't finesse me"
          )
# create a regex pattern
pattern1 <- "fi+ne"
```

This will return the numerical index for matches

```{r}
#| eval: TRUE
#| echo: TRUE

grep(pattern1, texts)
```

This will return a logical, TRUE for matches

```{r}
#| eval: TRUE
#| echo: TRUE
grepl(pattern1, texts)
```

## Wrangling Words

### Regular Expression (Regex)

-   Example:
    -   We want to find every text with "fine"
    -   We don't want "finesse" or "define"

## Wrangling Words

\small

```{r}
#| eval: TRUE
#| echo: TRUE
texts <- c(
          "this is fine",
          "this is fiiiine",
          "never gonna give you up",
          "it's not fine",
          "no need to define",
          "don't finesse me"
          )

# use word boundaries \b
pattern2 <- "\\bfi+ne\\b"
```

This returns the line number where there are words that begin with `fi` and end in `ne`:
```{r}
#| eval: TRUE
#| echo: TRUE
grep(pattern2, texts)
```

## Wrangling Words

### Regular Expression (Regex)

```{r}
#| echo: TRUE
#| eval: FALSE
pattern2 <- "\\bfi+ne\\b"
```
-   Special characters
    -   `\b` means "word boundary"
        -   word boundary is between a `\w` and `\W`
        -   `\w` is a any "word character"
        -   `\W` is a any "non-word character"
        
        
## Wrangling Words

### Regular Expression (Regex)
```{r}
#| echo: TRUE
#| eval: FALSE
pattern2 <- "\\bfi+ne\\b"
```
- Why the extra backslashes?
  - In `R` we have to "escape" backslashes[^1]

[^1]: This gets annoying, but is necessary. `R` will try to *interpret* a backslash, but we want it to "pass" the backslash to the regex engine, so the first backslash tells `R` not to interpret the second backslash.

## Wrangling Words {.smaller}

| Regex |                                    |
|------:|:----------------------------------:|
|    \\ |  escapes a special character       |
|    \^ |          string start              |
|    \$ |           string end               |
|     . |    any character but newline       |
|    \| |               or                   |
|     ? |  0 or 1 of previous character      |
|    \* | 0 or more of previous character    |
|    \+ | 1 or more of previous character    |
| \[ \] |     range of characters            |
|   ( ) |              group                 |
Table: Common Regex Special Characters

## Wrangling Words {.smaller}

| Regex |                                    |
|------:|:----------------------------------:|
|   \\w |  word character                    |
|   \\W |  non-word character                |
|   \\d |  digit (number)                    |
|   \\D |  non-digit                         |
Table: Backslash is also paired with some letters to create a special character

## Wrangling Words

### Regular Expression (Regex)

-   Whitespace and Non-Printing Characters
    -   Characters that are often hidden in WYSIWYG

## Wrangling Words

![Show NPCs in Microsoft Word](../images/img_word_formatting.png)

## Wrangling Words {.smaller}

|                  | Regex  |                      |
|-----------------:|:-------|:--------------------:|
|              Tab | \\t    |                      |
|          Newline | \\n    |                      |
|  Carriage Return | \\r    |                      |
|            Space | \\s    | Includes \\t \\n \\r |
|   Line separator | \\r\\n |                      |
| Horizontal space | \\h    |                      |
|     Vertical tab | \\v    |                      |
|    Word boundary | \\b    |                      |
Table: Most Common Non-Printing Characters

## Wrangling Words

Let's replace one or more whitespaces with just one space:

```{r}
#| eval: TRUE
#| echo: TRUE
df_cmu <- df_cmu |> 
      mutate(text_ws = gsub("\\s+", " ", text))

dtm_ws <- df_cmu |> 
             dtm_builder(text = text_ws, 
                         doc_id = doc_id)

dim(dtm_ws)
```
```{r}
#| eval: TRUE
#| echo: TRUE
# compare number of columns
ncol(dtm) - ncol(dtm_ws)
```


## Wrangling Words

### Replacing

-   What should we do with contractions?
    -   Remember the orphaned single letter "s" and "t"?

## Wrangling Words

### Contractions

-   What should we do with contractions?
    -   Before removing all punctuation
    -   Match only apostrophes
    -   Don't replace the apostrophe with a space
    -   Instead, replace it with no space at all
    -   "smoosh" the letters together

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE
text <- "You've gone? Hadn't the time. It's Jack's!"

gsub("'", "", text)

```

## Wrangling Words

### Contractions

-   Use a *dictionary* to replace contractions with their expanded form

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE
library(lexicon)
```

```{r}
#| eval: FALSE
#| echo: TRUE
data(package="lexicon")
```

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE
data(key_contractions, package = "lexicon")
dim(key_contractions)
```

```{r}
#| eval: TRUE
#| echo: TRUE
head(key_contractions)
```

## Wrangling Words

The `replace_contraction` function uses the `key_contractions` behind the scenes:

```{r}
#| eval: TRUE
#| echo: TRUE
library(textclean)

df_cmu <- df_cmu |> 
      mutate(text_co = replace_contraction(text))

dtm_co <- df_cmu |> 
          dtm_builder(text = text_co, 
                      doc_id = doc_id)

dim(dtm_co)
```

## Wrangling Words

### Replacing

-   Dictionary methods
    -   The backbone of "spell checkers"
    -   Finds a word NOT in the dictionary
    -   Suggests the word with the shortest "edit distance"

## Wrangling Words

Let's put all the steps together

\footnotesize

```{r}
#| eval: TRUE
#| echo: TRUE
df_cmu <- df_cmu |> 
  mutate(text_all = tolower(text),
         text_all = replace_contraction(text_all),
         text_all = gsub("[[:punct:]]", " ", text_all),
         text_all = gsub("\\s+", " ", text_all))

dtm_all <- df_cmu |> 
           dtm_builder(text = text_all, 
                       doc_id = doc_id)

dim(dtm_all)
```
```{r}
#| eval: TRUE
#| echo: TRUE
# compare number of columns
ncol(dtm) - ncol(dtm_all)
```

## Wrangling Words

### Beware:

-   Scunthorpe Problem
    -   inappropriately removes whole words because of "within-word" matches
-   Clbuttic Mistake
    -   inappropriately replacing "within-word" matches
-   The Cupertino Effect
    -   spell checker erroneously replaces words simply because they are not in its dictionary

## Wrangling Words

### Wrangling strategies:

-   Replacing (or Merging)
-   Removing

## Wrangling Words

### Removing Words

1.  Frequency Stopping
    -   Remove words above and below a frequency threshold
2.  Grammatical Stopping
    -   Remove words based on their part of speech
3.  Precompiled Stoplists
    -   Remove words from a pre-built dictionary

## Wrangling Words

### Removing Words

-   ~~Stopwords~~
-   There is no such thing as a stopword!

## Wrangling Words

### Removing Words

-   Stop words are
    -   any words that are “stopped” from consideration
    -   most use default pre-compiled stoplists
    -   most common comes from the 2014 version of the Snowball stemmer

## Wrangling Words

\footnotesize

```{r}
#| eval: TRUE
#| echo: TRUE
library(text2map)

sb_list <- get_stoplist(source = "snowball2014")
length(sb_list)
```

```{r}
#| eval: FALSE
#| echo: TRUE
sb_list
```
```         
  [1] "a"          "about"      "above"     
  [4] "after"      "again"      "against"   
  [7] "all"        "am"         "an"        
 [10] "and"        "any"        "are"       
 [13] "aren't"     "as"         "at"        
 [16] "be"         "because"    "been"      
 [19] "before"     "being"      "below"     
 [22] "between"    "both"       "but"       
 [25] "by"         "can't"      "cannot"
 ...
```

## Wrangling Words

### Removing Words

-   `grep` type functions
  -   either match exact words to remove from text
  -   or remove columns after creating the DTM

## Wrangling Words

\footnotesize

```{r}
#| eval: TRUE
#| echo: TRUE
doc <- c("It's the only thing that slowly stops the ache")
# tokenize on literal space
tkns <- unlist(str_split(doc, " "))
# create stoplist
my_stoplist <- c("that", "to", "the")
```

Which of $X$ is found in $Y$:

```{r}
#| eval: TRUE
#| echo: TRUE
# use the match operator
tkns %in% my_stoplist
```

## Wrangling Words

### Removing Words

-   Exclamation mark
    -   is an operator
    -   means **negation** or **not**
    -   `!=` means "not equal"
    -   `!TRUE` means "not true" (i.e. FALSE)

## Wrangling Words

Which of $X$ is NOT found in $Y$:

```{r}
#| eval: TRUE
#| echo: TRUE
!tkns %in% my_stoplist
```

## Wrangling Words

```{r}
#| eval: TRUE
#| echo: TRUE
# find logical index of keeps and stops
idx <- !tkns %in% my_stoplist
# use it to subset our token list
tkns <- tkns[idx]

tkns
```

## Wrangling Words

Stopping by removing unwanted columns:

```{r}
#| eval: TRUE
#| echo: TRUE
dtm <- df_cmu |> 
       dtm_builder(text = text, 
                   doc_id = doc_id)

idx <- !colnames(dtm) %in% my_stoplist
# remember DTMs are matrices with rows and columns
# put idx after the comma to indicate "columns"
dtm <- dtm[, idx]

```

## Wrangling Words

Using a dedicated stopper function:

```{r}
#| eval: TRUE
#| echo: TRUE
dtm <- df_cmu |> 
       dtm_builder(text = text, 
                   doc_id = doc_id)

dtm <- dtm |> 
       dtm_stopper(stop_list = my_stoplist)

```

## References {.allowframebreaks}
