library(text2map)
library(tidyverse)
library(stringi)
library(textclean)

## create a toy corpus
texts <- c(
          "knox in box",
          "fox in socks",
          "knox on fox in socks in box",
          "socks on knox and knox in box",
          "fox in socks on box on knox"
          )

corpus <- data.frame(
              text = texts, 
              line_id = paste0("line_", seq_along(texts))
          )

# create a DTM of our toy corpus
dtm <-  corpus |> 
        dtm_builder(text = text,
                   doc_id = line_id,
                   dense = TRUE)

# have a look at it
dtm

colSums(dtm)

rowSums(dtm)

library(gridExtra)
library(grid)

csum <- colSums(dtm) |> t() |> as.matrix()
rownames(csum) <- "TERM FREQ"
csum <- as.matrix(rbind(dtm, csum))

rsum <- rowSums(csum) |> as.matrix()
colnames(rsum) <- "LENGTH"
sub <- cbind(csum, rsum)

t1 <- ttheme_default(core=list(
        fg_params = list(fontface=c(rep("plain", 5), "bold.italic")),
        bg_params = list(fill=c(rep(c("grey95", "grey90"),
                                    length.out=5), "#35B779FF"),
                         alpha = rep(c(1,0.5), each=5))
        ))

sub |> grid.table(theme = t1)

data(corpus_cmu_blogs100, package = "text2map.corpora")

# rename
df_cmu <- corpus_cmu_blogs100
dim(df_cmu)

df_cmu <- df_cmu |> 
  mutate(
  text_all = stri_trans_general(text, id = "Any-Latin; Latin-ASCII"),
  text_all = tolower(text),
  text_all = replace_contraction(text_all),
  text_all = gsub("[[:punct:]]", " ", text_all),
  text_all = gsub("\\s+", " ", text_all)
  )

dtm <- df_cmu |> 
       dtm_builder(text = text_all, 
                   doc_id = doc_id)

dim(dtm)

# create stoplist
my_stoplist <- c("that", "to", "the")

dtm <- dtm |> 
       dtm_stopper(stop_list = my_stoplist)



docs <- c("at0833400_4.text", "mm0832300_0.text",
          "db0829400_6.text", "db0805200_2.text")

df_triple <- dtm_melter(dtm)

df_top <- df_triple |> 
  filter(doc_id %in% docs) |>
  group_by(doc_id) |> 
  slice_max(n = 10, order_by = freq) |>
  ungroup()

df_top |>
  mutate(term = reorder(term, freq)) |>
  ggplot(aes(x = term, y = freq, fill = doc_id)) +
     geom_col(show.legend = FALSE) + 
     coord_flip() +
     facet_wrap(~doc_id, scales = "free_y")



term_freqs <- sort(colSums(dtm), decreasing=TRUE)
freq_rank <- data.frame(freq = term_freqs) |>
             rownames_to_column(var = "term") |>
             rowid_to_column(var = "rank")


head(freq_rank, n = 10)

dtm_500 <- dtm |> 
          dtm_stopper(stop_termfreq = c(0, 500))

ncol(dtm) - ncol(dtm_500)

docs <- c("at0833400_4.text", "mm0832300_0.text",
          "db0829400_6.text", "db0805200_2.text")

df_triple <- dtm_melter(dtm_500)

df_triple |> 
  filter(doc_id %in% docs) |>
  group_by(doc_id) |> 
  slice_max(n = 10, order_by = freq) |>
  ungroup() |>
  mutate(term = reorder(term, freq)) |>
  ggplot(aes(x = term, y = freq, fill = doc_id)) +
     geom_col(show.legend = FALSE) + 
     coord_flip() +
     facet_wrap(~doc_id, scales = "free_y")


library(textstem)

docs <- c(
      "society is not the mere sum of individuals",
      "the system formed by their association",
      "it is from this combination that social life arises",
      "individuals give birth to a being"
      )

stem_strings(docs, language = "english")

library(lexicon)

data(hash_lemmas)

head(hash_lemmas)


filter(hash_lemmas, lemma == "be")

some_words <- c("be", "being", "was", "were", 
               "been", "am", "are", "is") 

lemmatize_words(some_words)


data(corpus_cmu_blogs100)

df_cmu <- corpus_cmu_blogs100 |> 
      mutate(text_st = stem_strings(text),
             text_lm = lemmatize_strings(text))


dtm    <- df_cmu |> dtm_builder(text = text, doc_id = doc_id)
dtm_st <- df_cmu |> dtm_builder(text = text_st, doc_id = doc_id)
dtm_lm <- df_cmu |> dtm_builder(text = text_lm, doc_id = doc_id)

dim(dtm)
dim(dtm_st)
dim(dtm_lm)
