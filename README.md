# text2map.slides

This repository has Quarto documents, lecture scripts, and rendered HTML files for slides corresponding with [*Mapping Texts: Computational Text Analysis for the Social Sciences*](https://textmapping.com/).

Note: We use `git-lfs` for non-plain text files (e.g., Rds files), so you need to install it before cloning: https://git-lfs.com/

- lecture01
    - lecture01_part01.Qmd: Text in Context
    - lecture01_part02.Qmd: Corpus Building
- lecture02
    - lecture02_part01.Qmd: Computing Basics
    - lecture02_part02.Qmd: Computing Basics, cont.
- lecture03
    - lecture03_part01.Qmd: Acquiring Texts
    - lecture03_part02.Qmd: Acquiring Texts, cont.
- lecture04
    - lecture04_part01.Qmd: Text to Numbers
    - lecture04_part02.Qmd: Text to Numbers, cont.
- lecture05
    - lecture05_part01.Qmd: Wrangling Words
    - lecture05_part02.Qmd: Wrangling Words, cont.
- lecture06
    - lecture06_part01.Qmd: Tagging Words: Part 1  (Gender Tagging)
    - lecture06_part02a.Qmd: Tagging Word: Part 2a (PoS Tagging)
    - lecture06_part02a.Qmd: Tagging Word: Part 2b (NER Tagging)
- lecture07
    - lecture07_part01.Qmd: Core Deductive
    - lecture07_part02.Qmd: Core Deductive, cont.
- lecture08
    - lecture08_part01.Qmd: Core Inductive: Part 2a (SVD)
    - lecture08_part02.Qmd: Core Inductive: Part 2b (LDA)
- lecture09
    - lecture09_part01a.Qmd: Extended Inductive: Part 1a (CTM)
    - lecture09_part01b.Qmd: Extended Inductive: Part 1b (STM)
    - lecture09_part02a.Qmd: Extended Inductive: Part 2a (SVD Embeddings)
    - lecture09_part02b.Qmd: Extended Inductive: Part 2b (GloVe Pre-Trained Embeddings)
- lecture10
    - lecture10_part01.Qmd: Extended Deductive: Part 1 (Supervised Learning)
    - lecture10_part02a.Qmd: Extended Deductive: Part 2a (Neural Nets)
    - lecture10_part02b.Qmd: Extended Deductive: Part 2b (Neural Nets w/ Pretrained Embeddings)
- lecture11
    - lecture11_part01.Qmd: Language Modeling Part 1