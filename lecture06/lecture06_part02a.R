library(tagger)

penn_tags()

para <- "Star Trek: Picard, which will debut early in 
         January 2020 on CBS All Access, sees Patrick 
         Stewart reprise his role as the noble leader 
         more than 20 years after Star Trek: The Next 
         Generation ended. The trailer shows Picard 
         retired at a vineyard on Earth, but feeling 
         out of place and longing for space travel"


para <- "Star Trek: Picard, which will debut early in January 2020 on CBS All Access, sees Patrick Stewart reprise his role as the noble leader more than 20 years after Star Trek: The Next Generation ended. The trailer shows Picard retired at a vineyard on Earth, but feeling out of place and longing for space travel"

tag_pos(para)


tag_pos(para) |> as_basic()

library(text2map.corpora)
data("corpus_cmu_blogs100")
# rename it so it's tidier
df <- corpus_cmu_blogs100

library(tidyverse)

text_lib <- df |> filter(rating == "Liberal")
text_con <- df |> filter(rating == "Conservative")

dim(text_lib)
dim(text_con)

pos_lib <- tag_pos(text_lib$text) |> as_basic()
pos_con <- tag_pos(text_con$text) |> as_basic()

length(pos_lib)
length(pos_con)

df_lib <- pos_lib |> 
          unlist() |> 
          names() |>
          table(dnn = "pos") |> 
          data.frame() |>
          mutate(rating = "Liberal")

df_con <- pos_con |> 
          unlist() |>
          names() |>
          table(dnn = "pos") |> 
          data.frame() |>
          mutate(rating = "Conservative")


df_pos <- rbind(df_lib, df_con)

df_pos <- df_pos |>
  filter(Freq >= 2) |>
  mutate(pos = fct_reorder(pos, Freq))

df_pos |>
ggplot(aes(x = pos, y = Freq, fill = rating)) +
    geom_col() +
    facet_wrap(~rating) +
    coord_flip()


df_pos |>
ggplot(aes(x = pos, y = Freq, fill = rating)) +
    geom_col(color = "black") +
    facet_wrap(~rating) +
    coord_flip() + 
    scale_fill_manual(
    values = c("Liberal"="#2332A9",
              "Conservative"="#990000") 
    )

noun_lib <- pos_lib |> select_tags("noun")
noun_con <- pos_con |> select_tags("noun")

noun_lib <- noun_lib |>
            unlist() |>
            table(dnn = "noun") |>
            data.frame() |>
            slice_max(Freq, n = 10) |>
            mutate(rating = "Liberal")

noun_con <- noun_con |>
            unlist() |>
            table(dnn = "noun") |>
            data.frame() |>
            slice_max(Freq, n = 10) |>
            mutate(rating = "Conservative")


noun_lib

noun_con
