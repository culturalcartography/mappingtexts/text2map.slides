library(text2map)
library(text2vec)
library(tidyverse)
library(stringi)
library(quanteda)

ppmi <- function(tcm, counts) {

    n_words <- sum(counts)
    tcm <- as(tcm, "TsparseMatrix")
    n_i <- counts[tcm@i + 1L] # count prefix
    n_j <- counts[tcm@j + 1L] # count suffix
    n_ij = as.integer(tcm@x) # co-occurence count
    # calculate pmi
    tcm@x <- log(n_ij) - log(n_i) - log(n_j) + log(n_words)
    # zero any negatives
    tcm[tcm < 0 ] <- 0
    return(tcm)
}




base_url <- "https://github.com/robertsdionne/rwet/raw/"
text_url <- "refs/heads/master/hw2/drseuss.txt"

cathat <- readLines(url(paste0(base_url, text_url)))
## have a look
head(cathat, 4)

library(stringi)
# tokenize
tkns <- cathat |> 
    stri_trans_general(id = "Any-Latin; Latin-ASCII") |> 
    tolower() |>
    tokens()

# build tcm
tcm_cathat <- fcm(tkns, 
                 context = "window", 
                 window = 1L, 
                 ordered = TRUE)

word_freqs <- meta(tcm_cathat, type = "object")$margin


tcm_cathat_ppmi <- ppmi(tcm_cathat, word_freqs)
dim(tcm_cathat_ppmi)

svd_cathat <- svd(tcm_cathat_ppmi)

# let's get the first 100 dimensions
wv_cathat <- svd_cathat$v[, 1:100]
rownames(wv_cathat) <- rownames(tcm_cathat_ppmi)
wv_cathat <- normalize(wv_cathat, "l1")


word <- "knox"
sims <- sim2(wv_cathat[word, , drop = FALSE], wv_cathat)

# we can only use positive values
sims[sims < 0] <- 0
# randomly sample from this distribution
idx <- sample(
            seq_along(sims), 
            size = 1,
            prob = sims
        )

# what is our next word?
colnames(sims)[idx]


seq <- c("socks", "on", "box", "on")
seq_vec <- colMeans(wv_cathat[seq, ])

sims <- sim2(t(as.matrix(seq_vec)), wv_cathat)
# we can only use positive values
sims[sims < 0] <- 0
# randomly sample from this distribution
idx <- sample(
            seq_along(sims), 
            size = 1,
            prob = sims
        )

# what is our next word?
colnames(sims)[idx]


next_word <- function(seq, vecs) {
    # weight by distance from predicted word
    len <- rev(1 / seq_along(seq))
    seq_vec <- vecs[seq, , drop = FALSE] * len
    seq_vec <- colMeans(seq_vec)
    sims <- sim2(t(as.matrix(seq_vec)), vecs, method = "cosine")
    # we can only use positive values
    sims[sims < 0] <- 0
    # randomly sample from this distribution
    idx <- sample(seq_along(sims), size = 1, prob = sims)
    # what is our next word?
    return(colnames(sims)[idx])
}


next_word(seq, wv_cathat)


gen_words <- function(vecs, seed, n, len) {

    ls_out <- list()
    length(ls_out) <- n
    # starting word
    ls_out[seq_along(seed)] <- seed

    for(idx in length(seed):n) {
        # only keep 3 most recent words
        win <- seq_len(idx)
        if(idx > len) win <- rev(sort(win, decreasing = TRUE)[seq_len(len)])
        seq <- unlist(ls_out[win])
        ls_out[[idx+1]] <- next_word(seq, vecs)
    }
    return(
        paste0(unlist(ls_out), collapse = " ")
    )
}

seed <- c("the", "grinch", "is")

gen_words(wv_cathat, seed, n = 12, len = 5)


library(text2map.pretrained)

data("vecs_glove300_wiki_gigaword")
# rename to save typing
wv <- vecs_glove300_wiki_gigaword
rm(vecs_glove300_wiki_gigaword)

dim(wv)

# we could train our own...
text <- cathat |> 
    stri_trans_general(id = "Any-Latin; Latin-ASCII") |> 
    tolower()

iterate <- itoken(word_tokenizer(text))
vocab_vec <- vocab_vectorizer(create_vocabulary(iterate))
tcm <- create_tcm(iterate, vocab_vec, skip_grams_window = 10L)

g_model <- GlobalVectors$new(rank = 100, x_max = 20)
glvecs <- g_model$fit_transform(tcm, n_iter = 50)
glvecs <- glvecs + t(g_model$components)

# to "encode" our vocabulary we need to match by word
match <- intersect(rownames(wv), rownames(tcm_cathat))
wv_ch <- wv[match, ]

seed <- c("sheep", "are", "some", "of", "the")

gen_words(wv_ch, seed, n = 20, len = 8)

