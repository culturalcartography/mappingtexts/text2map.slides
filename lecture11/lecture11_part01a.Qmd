---
title: "Language Modeling: Part 1a"
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

ptm <- proc.time()


```

## Language Modeling

### Setup
- Open an RStudio session
- Follow along the lecture script: `lecture11_part01a.R`


## Language Modeling

- Language models
    - Assign a probability to the *next* token
        - Can be used to **generate** text
        - Can be used to **suggest corrections**
        - Can be used to **model** how humans speak



## Language Modeling


![Young Sheldon using Eliza](../images/img_chatbot_eliza_youngsheldon.png)


## Language Modeling

>"ELIZA  is  a  program  operating  within  the  MAC  time-sharing  system  at  MIT  which makes  certain  kinds  of  natural  language  conversation  between  man  and  computer  possible.  Input  sen-  tences  are  analyzed  on  the  basis  of  decomposition  rules  which  are  triggered  by  key  words  appearing  in  the  input  text.  Responses are  generated  by  reassembly  rules  associated  with  selected  decomposition  rules" [@Weizenbaum1966-ou, 36]


## Language Modeling

```{r}
#| eval: TRUE
#| echo: TRUE
#| purl: TRUE
source("eliza.R")

```

```{r}
#| eval: FALSE
#| echo: TRUE
#| purl: TRUE
input <- ""
cat("Eliza: Hello, I am Eliza!\n")
while (TRUE) {
  input <- readline("You: ")
  if (input == "quit") break
  cat("Eliza:", Eliza(input), "\n")
}

```
```
You: Hi! My parents are fighting.
Eliza: Tell me more about your parents 
You: My mom bought this expensive computer and my father thinks we cannot afford it.
Eliza: Why do you mention computers? 
You: Because my mother bought one.
Eliza: Does that reason seem to explain anything else? 
You: Maybe my dad is right.
Eliza: I see. Can you elaborate on that? 
You: I guess I want dad to be nicer to mom.
Eliza: Why do you want dad to be nicer to mom? 
```

## Language Modeling

Let's look at the "model" object. 
```{r}
#| eval: TRUE
#| echo: TRUE

names(model)

```

## Language Modeling

This is actually just a dictionary!

If the model sees the key phrase "I feel" it will respond with:
```{r}
#| eval: TRUE
#| echo: TRUE

model$`I felt`

```

## Language Modeling

Some entries have multiple responses:
```{r}
#| eval: TRUE
#| echo: TRUE

model$`I want`

```

Some responses have placeholders to parrot what was said.


## Language Modeling

- Language models
    - This is a dictionary and rule-based language model 
    - We could imagine improving it by adding larger "decision trees"
    - But, language is sparse: we're always saying new things!


## Language Modeling

- Language models
    - Statistical language models: try to predict the next token by looking at large amounts of existing tokens.


## Language Modeling

What word is likely to follow: 

    fox in socks on box on ...


## Language Modeling

What word is likely to follow: 

    fox in socks on box on ...

How probable is "knox"?

## Language Modeling

What word is likely to follow: 

    fox in socks on box on ...

How probable is "knox"?

- We could count instances of "fox in socks on box on"
- Then count how many are followed by "knox"

## Language Modeling

The probability that "knox" follows fox in socks on box on is:

$\frac{Count\ of\ 'fox\ in\ socks\ on\ box\ on\ knox'}{Count\ of\ 'fox\ in\ socks\ on\ box\ on'}$


## Language Modeling

- The problem of sparsity
    - "fox in socks on box on" rarely occurs 
    - even if we had a corpus of everything ever written!

## Language Modeling

- The problem of sparsity
    - The **chain rule of probability**:
        We can estimate the *joint probability of a sequence* by multiplying together the *conditional probabilities of subsets* of that sequence

## Language Modeling

    knox in box
    fox in socks
    knox on fox in socks in box
    socks **on knox** and knox in box
    fox in socks on box **on knox**

The probability that "knox" follows "on" is:

$P(knox|on) = \frac{'on\ knox'}{total\ words} = \frac{2}{27} = 0.074$



    
## Language Modeling

Let's get the probabilities of all the bigrams

```{r}
#| eval: TRUE
#| echo: TRUE

library(stringr)

texts <- c(
          "knox in box",
          "fox in socks",
          "knox on fox in socks in box",
          "socks on knox and knox in box",
          "fox in socks on box on knox"
    )



```

We will add special starting STR and ending END tokens to count the tokens at the beginning and end.

## Language Modeling

We also want to only count words that **follow** another word


```{r}
#| eval: TRUE
#| echo: TRUE
library(tokenizers)
library(quanteda)

# tokenize
tkns <- tokens(texts)

tcm_seuss <- fcm(tkns, 
                 context = "window", 
                 window = 1L, 
                 ordered = TRUE)

counts <- meta(tcm_seuss, type = "object")$margin


```

`ordered = TRUE` means we only count when the column term occurs before the row term

## Language Modeling {.smaller}


    knox in box
    fox in socks
    knox on fox in socks in box
    socks **on knox** and knox in box
    fox in socks on box **on knox**

Checks out:
```{r}
#| eval: TRUE
#| echo: TRUE
tcm_seuss["on", "knox"]
```



## Language Modeling

- Built Term-Context Matrix (TCM)
   - How many times a word occurs in the same $n$-term window as another word
   - But, is that a lot of co-occurrences? Let's convert it to *probabilities*

## Language Modeling

We want to convert our co-occurrences to probabilities by dividing by the term frequencies in the corpus:

```{r}
#| eval: TRUE
#| echo: TRUE

## divide by all the occurrences in the corpus
tcm_seuss_probs <- tcm_seuss / sum(counts)

```

## Language Modeling {.smaller}

Have a look:
```{r}
#| eval: TRUE
#| echo: TRUE

tcm_seuss_probs
```

Recall:

$P(knox|on) = \frac{'on\ knox'}{total\ words} = \frac{2}{27} = 0.074$



## Language Modeling

- Built Term-Context Matrix (TCM)
  - The probability a word follows a word (or words)
  - We should "control" for how frequent both terms are


## Language Modeling


- Built Term-Context Matrix (TCM)
  - The probability a word follows a word (or words)
  - We should "control" for how frequent both terms are
  - Weight the TCM by "Positive Pointwise Mutual Information" (PPMI)


## Language Modeling

- "Pointwise Mutual Information" (PMI)
    - probability of $word_1$ following another $word_2$ 
    - divided by probability of each word occurring at all


## Language Modeling

    knox in box
    fox in socks
    knox on fox in socks in box
    socks on knox and knox in box
    fox in socks on box on knox

The conditional probability that "knox" follows "on" is:

$P(knox|on) = \frac{'on\ knox'}{total\ words} = \frac{2}{27} = 0.074$

The independent probability is:

$P(knox) = \frac{knox}{total\ words} = \frac{5}{27} = 0.1851$



## Language Modeling {.smaller}

    knox in box
    fox in socks
    knox on fox in socks in box
    socks on knox and knox in box
    fox in socks on box on knox

The probability that "in" follows "fox" is:

$P(knox|on) = \frac{'on\ knox'}{total\ words} = \frac{2}{27} = 0.074$

The independent probabilities are:

$P(knox) = \frac{knox}{total\ words} = \frac{5}{27} = 0.1851$

$P(on) = \frac{on}{total\ words} = \frac{4}{27} = 0.1481$

Thus:

$PMI(knox|on) = \frac{0.074}{0.1851 * 0.1481} = 2.6994$


## Language Modeling {.smaller}

    knox in box
    fox in socks
    knox on fox in socks in box
    socks on knox and knox in box
    fox in socks on box on knox

The PMI is:

$PMI(knox|on) = \frac{0.074}{0.1851 * 0.1481} = 2.6994$

This means the "knox" follows "on" more than we'd expect by chance!



## Language Modeling

- "Pointwise Mutual Information" (PMI)
    - probability of two words co-occurring
    - divided by probability of each word occurring at all
    - ...then we get the log^["...probabilities are (by definition) less than or equal to 1, and so the more probabilities we multiply together, the smaller the product becomes. Multiplying enough n-grams together would result in numerical underflow. Adding in log space is equivalent to multiplying in linear space, so we combine log probabilities by adding them" [@Jurafsky2021-ws, ch. 3, p. 6].]


## Language Modeling {.smaller}

    knox in box
    fox in socks
    knox on fox in socks in box
    socks on knox and knox in box
    fox in socks on box on knox

The PMI is:

$PMI(knox|on) = \log{\frac{0.074}{0.1851 * 0.1481}} = 0.993$

## Language Modeling

- **Positive**-Pointwise Mutual Information (PPMI)
  - PMI score could range from negative $\infty$ to positive $\infty$
  - But, negative numbers cause hassles, so we set all negatives to zero^[Negative numbers "imply things are co-occurring less often than we would expect by chance... [and] tend to be unreliable unless our corpora are enormous... and it's not clear whether it's even possible to evaluate such scores of 'unrelatedness' with human judgments" [@Jurafsky2021-ws, ch. 6, p. 14]]


## Language Modeling

```{r}
#| eval: FALSE
#| include: FALSE
#| purl: FALSE

tcm1 <- tcm_seuss
## this is from text2vec
n_words <- sum(counts)
tcm1 <- as(tcm1, "TsparseMatrix")
n_i <- counts[tcm1@i + 1L] # count prefix
n_j <- counts[tcm1@j + 1L] # count suffix
n_ij = as.integer(tcm1@x) # co-occurence count

## these are equivalent pmi methods
out1 <- log2((n_ij / nword) / ((n_i / nword) * (n_j / nword)))
out2 <- log2(n_ij) - log2(n_i) - log2(n_j) + log2(nword)
all.equal(out1, out2)

```

```{r}
#| eval: TRUE
#| echo: TRUE


ppmi <- function(tcm, counts) {

    n_words <- sum(counts)
    tcm <- as(tcm, "TsparseMatrix")
    n_i <- counts[tcm@i + 1L] # count prefix
    n_j <- counts[tcm@j + 1L] # count suffix
    n_ij = as.integer(tcm@x) # co-occurrence count
    # calculate pmi
    tcm@x <- log(n_ij) - log(n_i) - log(n_j) + log(n_words)
    # zero any negatives
    tcm[tcm < 0 ] <- 0
    return(tcm)
}




```



## Language Modeling {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE

tcm_ppmi <- ppmi(tcm_seuss, counts)
round(tcm_ppmi, 3)
```

## Language Modeling

- Language model assigns a probability to the *next* word
    - We have to convert ppmi back to probabilities
    - Then sample from that probability distribution to *generate* probable sequences

## Language Modeling

```{r}
#| eval: TRUE
#| echo: TRUE

next_word <- function(word, tcm) {
        
    word_idx <- seq_len(ncol(tcm))
    prob_dis <- tcm[, word]
    idx <- sample(word_idx, 
            size = 1,
            prob = prob_dis)
    return(colnames(tcm)[idx])
}

next_word("knox", tcm_ppmi)

```

## Language Modeling {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE

gen_words <- function(tcm, word, n) {

    ls_out <- list()
    length(ls_out) <- n
    # starting word
    ls_out[[1]] <- word

    for(idx in seq_len(n)) {
        ls_out[[idx+1]] <- next_word(ls_out[[idx]], tcm)
    }
    return(
        paste0(unlist(ls_out), collapse = " ")
    )
}

gen_words(tcm_ppmi, "knox", n = 10)

```

## Language Modeling

Let's grab the entire book now
```{r}
#| eval: TRUE
#| echo: TRUE


base_url <- "https://github.com/robertsdionne/rwet/raw/"
text_url <- "refs/heads/master/hw2/drseuss.txt"

cathat <- readLines(url(paste0(base_url, text_url)))
## have a look
head(cathat, 4)
```

## Language Modeling

```{r}
#| eval: TRUE
#| echo: TRUE

library(stringi)
# tokenize
tkns <- cathat |> 
    stri_trans_general(id = "Any-Latin; Latin-ASCII") |> 
    tolower() |>
    tokens()

# build tcm
tcm_cathat <- fcm(tkns, 
                 context = "window", 
                 window = 1L, 
                 ordered = TRUE)

counts <- meta(tcm_cathat, type = "object")$margin


tcm_cathat_ppmi <- ppmi(tcm_cathat, counts)
dim(tcm_cathat_ppmi)
```

## Language Modeling
```{r}
#| eval: TRUE
#| echo: TRUE

gen_words(tcm_cathat_ppmi, "sheep", n = 10)
```

## Language Modeling

- N-gram language model
    - We could increase the accuracy of this model by modeling trigrams, on up...

## Language Modeling

- N-gram language model
    - We could increase the accuracy of this model by modeling trigrams, on up...
- The problem of sparsity
    - The larger the n-gram, the less it will occur


## Language Modeling

- N-gram language model
    - We could increase the accuracy of this model by modeling trigrams
- The problem of sparsity
    - The larger the n-gram, the less it will occur
- The **locality problem**
    - N-gram models will fail to capture "long-range" dependencies



## Language Modeling

This only requires a 2-gram:

> **stocks fell** sharply as a reulst of the announcement

This would require a 9-gram

> **stocks**, as a result of the announcement, sharply **fell**


## Language Modeling

    knox in box
    fox in socks
    knox on fox in socks in box
    socks on knox and knox in box
    fox in socks on box on knox

Number of unique 1-grams:
```{r}
#| echo: FALSE
#| eval: TRUE

all <- paste0(texts, collapse = " ")
length( unique(unlist(tokenize_ngrams(all, n = 1))) )

```

Number of unique 2-grams:
```{r}
#| echo: FALSE
#| eval: TRUE

length( unique(unlist(tokenize_ngrams(all, n = 2))) )

```

Number of unique 4-grams:
```{r}
#| echo: FALSE
#| eval: TRUE

length( unique(unlist(tokenize_ngrams(all, n = 4))) )

```


## Language Modeling

- N-gram language model
    - Better with larger corpora and larger n-grams...
    - But these models don't "scale up" well!

## Language Modeling

- What if we incorporate words that "behave like" our focal word?
- This will increase the examplars we do find...
- We can do this with **word embeddings**!
 
## Slide Information

Time to render (in seconds):
```{r}
#| eval: TRUE
#| echo: FALSE 
#| purl: FALSE
out <- proc.time() - ptm
as.list(out)$elapsed

```




## References {.allowframebreaks}