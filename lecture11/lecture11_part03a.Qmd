---
title: "Language Modeling: Part 3"
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

ptm <- proc.time()


```


## Language Modeling

- Language models
    - Assign a probability to each *next* word
        - Can be used to **generate** text
        - Can be used to **suggest corrections**
        - Can be used to **model** how humans speak

## Language Modeling

- Language models
    - They are just prediction machines based on our training data


## Language Modeling


![](../images/img_openai_chatgpt_strawberry.png)


## Language Modeling


![](../images/img_openai_chatgpt_nonbinary.png)


## Language Modeling

- Language models
    - We can *pretrain* these models and use them later
    - We only have to spend these initial computational resources once...

## Language Modeling

- Pretrained language models
    - Models are just *weights on features* 
    - In text analysis, the features are tokens

## Language Modeling

- Pretrained language models
    - To associate our input text with the appropriate weights, we need to tokenize the new input text
    - But, this tokenizing needs to match how the training text was tokenized


## Language Modeling

- Pretrained language models
    - Models are weights on features, **and a tokenizer (or preprocessor)**
    - For example, OpenAI, the company that provides ChatGPT has a specific tokenizer:
        - https://platform.openai.com/tokenizer


## Language Modeling

>"All men hate the wretched; how, then, must I be hated, who am miserable beyond all living things! Yet you, my creator, detest and spurn me, thy creature, to whom thou art bound by ties only dissoluble by the annihilation of one of us. You purpose to kill me. How dare you sport thus with life? Do your duty towards me, and I will do mine towards you and the rest of mankind. If you will comply with my conditions, I will leave them and you at peace; but if you refuse, I will glut the maw of death, until it be satiated with the blood of your remaining friends." [@Shelley1869-sp]

## Language Modeling


![How OpenAI's GPT-4o model tokenizes](../images/img_openai_tokenizer1.png)


## Language Modeling


![Each colored section corresponds to a unique index](../images/img_openai_tokenizer2.png)

## Language Modeling


![](../images/img_openai_tokenizer3a.png)
![](../images/img_openai_tokenizer4a.png)


## Language Modeling


```{python}
#| eval: FALSE
#| echo: TRUE
#| purl: FALSE

import tiktoken
# get specific encoder
enc = tiktoken.get_encoding("o200k_base")
# encode then decode
enc.decode(enc.encode("hello world"))

```



## Language Modeling


```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE
#| warning: FALSE

library(reticulate)

tiktoken <- import("tiktoken")

# get a specific encoder
enc <- tiktoken$get_encoding("o200k_base")
# encode text
encoded_text <- enc$encode("Hello people of Earth!")

encoded_text
```

```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE
#| warning: FALSE

# decode text
decoded_text <- enc$decode(encoded_text)

decoded_text
```


## Language Modeling

```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE
#| warning: FALSE

library(reticulate)

khub <- import("keras_hub")$models

# get the preprocessor for GPT-2
preprocessor <- khub$GPT2CausalLMPreprocessor$from_preset(
    "gpt2_base_en", sequence_length = 128,
)

# get the model for GPT-2 with preprocessor
gpt2_lm <- khub$GPT2CausalLM$from_preset(
    "gpt2_base_en", preprocessor = preprocessor
)


```

## Language Modeling

```{r}
#| echo: TRUE
#| eval: TRUE
#| message: FALSE
#| warning: FALSE

input <- c("The extraterrestrial exited the spacecraft and ")
# generate response
output <- gpt2_lm$generate(input, max_length = 100L)

cat(output)

```



## Language Modeling

- Pretrained language models
    - Pretrained models are often LARGE  - Large Language Models (LLMs)
    - Typically, developers release different SIZES
    - Sizes are measured in terms of parameters

## Language Modeling

- Pretrained language models
    - Model size (in part) is dimensionality and layers
        - Use the same basic architecture, reduce the dimensionality and layers 

## Language Modeling

- Pretrained language models
    - Model size (in part) is dimensionality and layers
        - GPT-2 Extra Large: 1600 x 48
        - GPT-2 Large: 1280 x 36
        - GPT-2 Medium: 1024 x 24
        - GPT-2 Small: 768 x 12

## Language Modeling

- Pretrained language models
    - Model size (in part) is dimensionality and layers
    - Distillation [@Xu2024-st]: 
        - Training a smaller model (the student) to mimic the larger model (the teacher)

## Language Modeling

- Pretrained language models
    - Model size (in part) is dimensionality and layers
    - Distillation [@Xu2024-st]: 
    - Quantization [@Gholami2021-ac]:
        - Roughly, we round up the numbers in the weights
        - We reduce 32-bit floating points to 8-bit or 4-bit integers
        - Most models are trained on 16-bit now

## Language Modeling

- Pretrained language models
    - Properietary vs Open
        - ChatGPT is currently powered by the GPT-4o model
        - You can only access this model through OpenAI


## Language Modeling

- Pretrained language models
    - Properietary vs Open
        - ChatGPT is currently powered by the GPT-4o model
        - You can only access this model through OpenAI
        - Google's Gemini is powered by the Gemma model
        - You can download all the Gemma models


## Language Modeling

- Pretrained language models
    - Llama by Meta (Facebook)
    - Provided a utility to run LLMs locally: Ollama
        - https://ollama.com/

## Language Modeling

- Pretrained language models
    - With Ollama, we can easily use LLMs in `R`
        - `ollamar` package
        - https://hauselin.github.io/ollama-r/

## Language Modeling

- Pretrained language models
    - With Ollama, we can easily use LLMs in `R`
        - `ollamar` package
        - https://hauselin.github.io/ollama-r/
    - We can also use open-source graphical user interfaces (GUIs)
        - gpt4all
        - https://gpt4all.io/

## Slide Information

Time to render (in seconds):
```{r}
#| eval: TRUE
#| echo: FALSE 
#| purl: FALSE
out <- proc.time() - ptm
as.list(out)$elapsed

```



## References {.allowframebreaks}