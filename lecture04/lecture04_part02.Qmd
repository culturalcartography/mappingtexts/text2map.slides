---
title: "Text to Numbers, Cont."
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

```

# From Text To Numbers: Weighting, Cont.

## From Text To Numbers

### Setup
- Open an RStudio session
- Follow along the lecture script: `lecture04_part02.R`

## From Text To Numbers

Load these packages:
```{r}
#| eval: TRUE
#| echo: TRUE
#| message: FALSE

library(tidyverse)
library(text2map)
library(text2map.corpora)
library(stringi)
```

## From Text To Numbers

Load a corpus
```{r}
#| eval: TRUE
#| echo: TRUE
#| message: FALSE

data(corpus_taylor_swift, package = "text2map.corpora")

# rename it `df_ts` to save some typing:
df_ts <- corpus_taylor_swift

```


## From Text To Numbers


Let's do a little bit of preprocessing (the same steps as last time)

\footnotesize
```{r}
#| eval: TRUE
#| echo: TRUE
#| 
df_ts <- df_ts |> 
  mutate(
  song_text = stri_trans_general(
              song_text, id = "Any-Latin; Latin-ASCII"),
  song_text = gsub("[[:punct:]]", " ", song_text)
  )
```

## From Text To Numbers

Build your DTM
```{r}
#| eval: TRUE
#| echo: TRUE
dtm <- df_ts |> 
     dtm_builder(text = song_text, 
                 doc_id = song_id)

```


## From Text To Numbers

Create a frequency/rank dataframe:

```{r}
#| eval: TRUE
#| echo: TRUE
term_freqs <- sort(colSums(dtm), decreasing=TRUE)

freq_rank <- data.frame(freq = term_freqs) |>
             rownames_to_column(var = "term") |>
             rowid_to_column(var = "rank")

```


## From Text To Numbers

Get top 10 terms
```{r}
#| eval: TRUE
#| echo: TRUE
head(freq_rank, n = 10)

```

## From Text To Numbers

Get bottom 10 terms
```{r}
#| eval: TRUE
#| echo: TRUE
tail(freq_rank, n = 10)

```

## From Text To Numbers

- What are the most frequent words in each document?
  - We are going to "melt" our DTM into a new data structure
  - This is a "tripletlist"
  - It will have three variables
    - doc_id
    - term
    - freq

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

df_triple <- dtm_melter(dtm)

```



## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

# get the top 10 terms in each song
df_top <- df_triple |> 
          group_by(doc_id) |> 
          slice_max(n = 10, order_by = freq)

```

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE
#| output-location: slide

# look at just "Hey Stephen"
df_top |> 
  filter(doc_id == "101934") |>
  mutate(term = reorder(term, freq)) |>
  ggplot(aes(x = term, y = freq)) +
     geom_col() + 
     coord_flip()
   
```



## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.cap: "Top terms in a selection of four Taylor Swift songs. What do we notice?"
#| purl: FALSE

songs <- c("101934", "124510",
           "124513" , "132077")

df_triple |> 
  filter(doc_id %in% songs) |>
  group_by(doc_id) |> 
  slice_max(n = 10, order_by = freq) |>
  ungroup() |>
  mutate(term = reorder(term, freq)) |>
  ggplot(aes(x = term, y = freq, fill = doc_id)) +
     geom_col(show.legend = FALSE) + 
     coord_flip() +
     facet_wrap(~doc_id, scales = "free_y")
```

## From Text To Numbers

Takeaway: The *most frequent* terms are usually not the *most informative* terms!


## From Text To Numbers

### TF-IDF
- Term frequency by inverse document frequency
- Shows how unique a term is to a document

## From Text To Numbers

### TF-IDF
- Document Frequency (DF)
  - In how many documents does a word appear?
  
## From Text To Numbers

Create a binary/boolean matrix
```{r}
#| eval: TRUE
#| echo: TRUE

dtm_bin <- dtm > 0

```

This is giving us: is a cell greater than zero (TRUE) or not (FALSE)


## From Text To Numbers

Then sum the columns^[When summing a logical, TRUE is treated as 1 and FALSE is treated as 0] to get each term's document frequency
```{r}
#| eval: TRUE
#| echo: TRUE

doc_freq <- colSums(dtm_bin)
head(doc_freq)
```


## From Text To Numbers

### TF-IDF
- In _Afterglow_
  - Swift uses **blew** 1 time
  - **blew** occurs in just 4 songs
  - $\frac{1}{4} = 0.25$
  - Swift uses **you** 18 times
  - **you** occurs in 116 songs
  - $\frac{18}{116} = 0.16$
 
 
## From Text To Numbers

\center 

The word "blew" is more distinctive of _Afterglow_ than the word "you" -- even though "you" occurs many more times than "blew" in _Afterglow_

## From Text To Numbers

### TF-IDF
- IDF = Total Documents / Term's Document Frequency
- It is common to "log" the idf
  - dampens the effect of high frequencies


## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

idf <- log(nrow(dtm)/doc_freq)
```


## From Text To Numbers

To weight a DTM by tf-idf:
```{r}
#| eval: TRUE
#| echo: TRUE

dtm_tfidf <- t(t(dtm) * idf)
```

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

dtm_tfidf[1:2, c("blew", "you")]
```

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

# melt this tf-idf weighted matrix
df_triple_tfidf <- dtm_melter(dtm_tfidf)

# grab the top ten terms, this time weighted by tf-idf
df_top_tfidf <- df_triple_tfidf |> 
          group_by(doc_id) |> 
          slice_max(n = 10, order_by = freq)

```

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE
#| output-location: slide

# look at just "Hey Stephen" again
df_top_tfidf |> 
  filter(doc_id == "101934") |>
  mutate(term = reorder(term, freq)) |>
  ggplot(aes(x = term, y = freq)) +
     geom_col() + 
     coord_flip()
   
```

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: FALSE
#| purl: FALSE

songs <- c("101934", "124510",
           "124513" , "132077")

df_top_tfidf |>
  filter(doc_id %in% songs) |>
  group_by(doc_id) |> 
  slice_max(n = 10, order_by = freq) |>
  ungroup() |>
  mutate(term = reorder(term, freq)) |>
  ggplot(aes(x = term, y = freq, fill = doc_id)) +
     geom_col(show.legend = FALSE) + 
     coord_flip() +
     facet_wrap(~doc_id, scales = "free_y")
```


# Term Features

## From Text To Numbers

- We've been looking at patterns at the corpus- or document-level
- Now we're going to consider how specific *tokens are used*


## From Text To Numbers


### Term-Context Matrix (TCM)
- Rows: Each unique term in the vocabulary
- Columns: A term's "context"
  1. Which documents does a term occur in?
  2. Which terms does a term co-occur with in a given window?



## From Text To Numbers

### Term-Context Matrix (TCM)
1. Which documents does a term occur in?
  - Just tip and twist our DTM (i.e. *transpose*)

## From Text To Numbers

:::: {.columns}
::: {.column width="40%"}

```{r}
#| eval: TRUE
#| echo: FALSE
#| purl: FALSE 
#| fig.align: center  
#| out.height: "50%"

m1 <- matrix(c("A", "B", "C", "D", "E", "F"), ncol = 2)
rownames(m1) <- paste0("doc_", seq_len(nrow(m1)))
colnames(m1) <- paste0("term_", seq_len(ncol(m1)))

m1
```

:::
::: {.column width="60%"}

```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.align: "center"
#| out.height: "100%"
#| purl: FALSE
t(m1)

```

:::
::::

\vspace{1cm}
The __tranpose__ of a matrix reverses the row and column dimensions -- it *tips and twists the matrix*.


## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

# transpose the Taylor Swift DTM
tcm <- t(dtm)

```


## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

# show how many times "love" occurs in each song
tcm["love", ]

```

## From Text To Numbers

### Term-Context Matrix (TCM)
- Two words are similar if: $word^1$ occurs in the same documents as $word^2$ and *does not* occur in the same documents as $word^2$ does not occur


## From Text To Numbers

### Term-Context Matrix (TCM)
- Cosine
  - Angle between two vectors
  - Cosine *similarity*: $1$ to $-1$ where 1 is exactly the same 
  - Standard way to measure if two vectors are similar



## From Text To Numbers

### Term-Context Matrix (TCM)

```{r}
#| eval: TRUE
#| echo: TRUE

# create a cosine similar function
cos_sim <- function(A, B) {
  sum(A * B) / sqrt(sum(A^2) * sum(B^2)) 
}

```

## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE

# get select the rows by word
vec1 <- as.vector(tcm["shake", ])
vec2 <- as.vector(tcm["love", ])
vec3 <- as.vector(tcm["alone", ])
cos_sim(vec1, vec2) # "shake" and "love" 
cos_sim(vec2, vec3) # "love" and "alone" 
cos_sim(vec3, vec1) # "alone" and "shake"

```

## From Text To Numbers

### Term-Context Matrix (TCM)
1. Which documents does a term occur in?
    - Just tip and twist our DTM (i.e. *transpose*)
2. Which terms does a term *co-occur* with in a given window?
    - "Term Co-Occurrence Matrix" -- also called a TCM
    - For today, we'll assume the window is an entire document


## From Text To Numbers

### Term-Context Matrix (TCM)
- Term Co-Occurrence Matrix
    - We will use "matrix multiplication" to count how many times two terms co-occur

## From Text To Numbers


:::: {.columns}
::: {.column width="50%"}


```{r}
#| eval: TRUE
#| echo: FALSE
#| message: FALSE
#| fig.align: center
#| out.height: 50%
#| purl: FALSE
library(kableExtra)
ppl <- c("doc_1", "doc_2", "doc_3")
int <- c("all", "out", "of", "bubblegum")

mat <- matrix(nrow = length(ppl), ncol = length(int), dimnames = list(ppl, int))

cmat <- mat
cmat[] <- c(2, 1, 1, 1,
            1, 0, 0, 1, 
            1, 1, 0, 0)

tb_cmat <- knitr::kable(cmat, caption = "Matrix A") |> 
column_spec (1:4,border_left = TRUE, border_right = TRUE) |>
kable_styling(font_size = 25)

tb_cmat
```

:::
::: {.column width="50%"}

```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.align: center
#| out.height: 100%
#| purl: FALSE
tmat <- t(cmat)
tb_tmat <- knitr::kable(tmat, caption = "Matrix A^T") |> 
column_spec (1:4,border_left = TRUE, border_right = TRUE) |>
kable_styling(font_size = 25)

tb_tmat
```

:::
::::

```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.align: center
#| out.height: 100%
#| purl: FALSE
rmat <- t(mat) %*% mat
rmat[] <- ""
tb_rmat <- knitr::kable(rmat, caption = "Matrix C") |> 
column_spec (1:4,border_left = TRUE, border_right = TRUE) |>
kable_styling(font_size = 25)

tb_rmat
```

\center 
Where do we get the numbers for the cells in Matrix C?


## From Text To Numbers

:::: {.columns}
::: {.column width="50%"}


```{r}
#| eval: TRUE
#| echo: FALSE
#| message: FALSE
#| fig.align: center
#| out.height: 50%
#| purl: FALSE
tb_cmat |> column_spec(2, color = 'black', background = 'yellow')

```

:::
::: {.column width="50%"}


```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.align: center
#| out.height: 100%
#| purl: FALSE
tb_tmat |> row_spec(2, color = 'black', background = 'yellow')

```

:::
::::


```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.align: center
#| out.height: 100%
#| purl: FALSE
rmat2 <- rmat

rmat2[1,2] <- "?"

tb_rmat2 <- knitr::kable(rmat2, caption = "Matrix A^T") |> 
column_spec (1:4,border_left = TRUE, border_right = TRUE) |>
kable_styling(font_size = 25)

tb_rmat2

```

Sum of __column cells__ multiplied by corresponding __row cells__

## From Text To Numbers

:::: {.columns}
::: {.column width="50%"}


```{r}
#| eval: TRUE
#| echo: FALSE
#| message: FALSE
#| fig.align: center
#| out.height: 50%
#| purl: FALSE
tb_cmat |> column_spec(2, color = 'black', background = 'yellow')


```

:::
::: {.column width="50%"}


```{r}
#| eval: TRUE
#| echo: FALSE
#| message: FALSE
#| fig.align: center
#| out.height: 50%
#| purl: FALSE
tb_tmat |> row_spec(2, color = 'black', background = 'yellow')

```

:::
::::

```{r}
#| eval: TRUE
#| echo: FALSE
#| fig.align: center
#| out.height: 100%
#| purl: FALSE
rmat3 <- rmat

rmat3[1,2] <- 3


tb_rmat3 <- knitr::kable(rmat3, caption = "Matrix A^T") |> 
column_spec (1:4,border_left = TRUE, border_right = TRUE) |>
kable_styling(font_size = 25)

tb_rmat3

```

all --> out $= (2 * 1) + (1 * 1) + (1 * 0) = 3$


## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE
#| purl: FALSE

docs <- c("doc_1", "doc_2", "doc_3")
term <- c("all", "out", "of", "bubblegum")
nums <- c(2, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0)

mat <- matrix(nums,
              ncol = length(term), 
              dimnames = list(docs, term))

## this takes care of matrix multiplication for us:
crossprod(mat)

```


## From Text To Numbers

```{r}
#| eval: TRUE
#| echo: TRUE
#| purl: FALSE

# find the crossprod of our Taylor Swift DTM
tcm <- crossprod(dtm)

# get select the rows by word
vec1 <- as.vector(tcm["shake", ])
vec2 <- as.vector(tcm["love", ])
vec3 <- as.vector(tcm["alone", ])
cos_sim(vec1, vec2) # "shake" and "love" 
cos_sim(vec2, vec3) # "love" and "alone" 
cos_sim(vec3, vec1) # "alone" and "shake"

```


## From Text To Numbers

### Term-Context Matrix (TCM)
1. Which documents does a term occur in?
    - Tranposed the DTM
    - Cosine of these vectors gets us **syntagmatic** similarities:  
      - Two terms literally are used together in the same contexts

## From Text To Numbers

### Term-Context Matrix (TCM)
2. Which terms does a term co-occur with in a given window?
    - Matrix Multiplication on the DTM
    - Cosine of these vectors gets **paradigmatic** similarities:  
      - Two terms are used in similar contexts, but might never co-occur


## References {.allowframebreaks}