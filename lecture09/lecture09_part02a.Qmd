---
title: "Extended Inductive: Embeddings"
subtitle: "Computational Text Analysis"
author: 
  - name: "Prof. Dustin Stoltz"
    affiliation: "Lehigh University"
  - name: "Prof. Marshall Taylor"
    affiliation: "New Mexico State University"
format: 
  revealjs:
     theme: moon
     embed-resources: true
slide-number: true
# transition: "slide"
bibliography: ../references.bib
citeproc: true
highlight-style: oblivion # a11y # vim-dark # 
---

```{r}
#| eval: TRUE
#| include: FALSE
#| purl: FALSE
knitr::purl(knitr::current_input(), documentation = 0)

```

## Extended Inductive

### Setup
- Open an RStudio session
- Follow along the lecture script: `lecture09_part02a.R`


## Extended Inductive
  
Check out:

https://semantle.novalis.org/


## Extended Inductive
  
Now check out:

https://neal.fun/infinite-craft/
  
## Extended Inductive

### Vector Space Model
- Document-Term Matrix as a Vector **Space**
  - Each document (rows) is represented by the relative frequency of unique words (columns)
  - Each column is a "dimension" in an $n$-dimensional space
  - Each word is one dimension
  - Each document is a location in the $n$-dimensional space

## Extended Inductive

### Vector Space Model
- Topic Models
  - Documents are defined by a vector of probabilities over topics
  - Topics are the dimensions in the "vector space"
  
# Word Embeddings

## Extended Inductive: Embeddings

- Our discussion has focused mostly on 
    - Representing the meanings of documents
    - Measuring how documents are associated
- What if we are more interested in 
    - The meanings of **terms**
    - The association between **terms**


## Extended Inductive: Embeddings

Let's review **matrix projection**



## Extended Inductive: Embeddings

- Matrix Projection:
  - M x t(M) = row by row 1-mode matrix
  - t(M) x M = col by col 1-mode matrix


## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE

texts <- c(
          "knox in box",
          "fox in socks",
          "knox on fox in socks in box",
          "socks on knox and knox in box",
          "fox in socks on box on knox"
          )

corpus <- data.frame(
              text = texts, 
              line_id = paste0("line_", seq_along(texts))
          )
```

## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE

library(text2map)

dtm_seuss <-  corpus |> 
        dtm_builder(text = text,
                   doc_id = line_id)

```


## Extended Inductive: Embeddings

"Transpose" tips and twists our matrix
```{r}
#| eval: TRUE
#| echo: TRUE
tdm_seuss <- t(dtm_seuss)
```

## Extended Inductive: Embeddings {.smaller}


:::::::::::::: {.columns}
::: {.column width="50%"}

Term-Document Matrix
```{r}
#| eval: TRUE
#| echo: TRUE
as.matrix(tdm_seuss)

```


:::
::: {.column width="50%"}

Document-Term Matrix
```{r}
#| eval: TRUE
#| echo: TRUE
as.matrix(dtm_seuss)

```

:::
::::::::::::::

## Extended Inductive: Embeddings


Creates a term-co-occurrence matrix (term by term) matrix (TCM)
```{r}
#| eval: TRUE
#| echo: TRUE
tcm_seuss <- tdm_seuss %*% dtm_seuss
```
Creates a document-similarity matrix (document by document) matrix (DSM)
```{r}
#| eval: TRUE
#| echo: TRUE
dsm_seuss <- dtm_seuss %*% tdm_seuss
```

## Extended Inductive: Embeddings

These are more efficient ways to calculate the previous.
```{r}
#| eval: TRUE
#| echo: TRUE

# this will create a TCM
tcm_seuss <- crossprod(dtm_seuss)
```
```{r}
#| eval: TRUE
#| echo: TRUE

# this will create a DSM
dsm_seuss <- tcrossprod(dtm_seuss)
```

## Extended Inductive: Embeddings {.smaller}

:::::::::::::: {.columns}
::: {.column width="50%"}

Term-Context Matrix
```{r}
#| eval: TRUE
#| echo: TRUE
as.matrix(tcm_seuss)

```


:::
::: {.column width="50%"}

Document-Similarity Matrix
```{r}
#| eval: TRUE
#| echo: TRUE
as.matrix(dsm_seuss)

```

:::
::::::::::::::

## Extended Inductive: Embeddings


- Diagonal
  - term's do *co-occur* with itself
    - "knox" occurs 5 times, but the diagonal is 7?
    - "knox in box" $= 1$
    - "socks on knox and knox in box" $= 4$
  - we only want co-occurrences once
    - "knox in box" $= 1$
    - "socks on knox and knox in box" $= 2$

## Extended Inductive: Embeddings


- Diagonal
  - we need to correct for a term's total frequency
    - total frequency less 1 (for a single occurrence in the corpus)

## Extended Inductive: Embeddings

Create a TCM out of our DTM correcting for self-occurrence:
```{r}
#| eval: TRUE
#| echo: TRUE

# get term's corpus frequency
freq <- colSums(dtm_seuss) - 1 
# matrix projection
tcm <-  crossprod(dtm_seuss)
# correct for self-occurrence
diag(tcm) <- diag(tcm) - freq
```

## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE

as.matrix(tcm)
```

## Extended Inductive: Embeddings


- These methods *can be* used to measure word similarities
  - When we start with the *document*-term matrix, we focus on **sharing documents**
- Why might that be a limitation if we are interested in the similarities of the meanings of words?

## Extended Inductive: Embeddings

- Similarities between terms based on the extent they occur in similar *contexts*
- DTMs define **word contexts** as the words occurring in the *same documents*

## Extended Inductive: Embeddings


- What if a "context" is a "window" of ~5--10 terms...
- Typically, both left and right of a target word
- Sometimes this is a "moving" window a.k.a. "shingle"
- This gets at more specific meanings of words
 
## Extended Inductive: Embeddings

Packages for loading and preparing data
```{r}
#| eval: TRUE
#| echo: TRUE
#| warning: FALSE
#| message: FALSE

library(tidyverse)
library(textclean)
library(stringi)
library(text2map.corpora)

```

## Extended Inductive: Embeddings

Load corpus and prepare the text

```{r}
#| eval: TRUE
#| echo: TRUE
#| warning: FALSE
#| message: FALSE

data("corpus_tng_season5")

# light preprocessing of the lines
df_tng <- corpus_tng_season5 |> 
mutate(text = replace_curly_quote(line),
       text = stri_trans_general(text, "Any-Latin; Latin-ASCII"),
       text = replace_contraction(text),
       text = tolower(text),
       text = gsub("[[:punct:]]+", " ", text),
       text = gsub("[[:digit:]]+", " ", text),
       text = gsub("\\s+", " ", text)) 

dim(df_tng)

```

## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE
df_tng <- df_tng |> 
      mutate(n_terms = str_count(text, "\\W+"))


min(df_tng$n_terms)
max(df_tng$n_terms)

```


## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE
#| output-location: slide
#| fig.cap: "Most lines are short!"
df_tng |> 
  ggplot(aes(x = n_terms)) +
  geom_density()
```



## Extended Inductive: Embeddings

- Let's "chunk" our lines into 10 word pseudo-documents
- Each row will be ten words, in the order they appear in the scripts

## Extended Inductive: Embeddings

This will create a DTM where each "document" is just a 10 word chunk:
```{r}
#| eval: TRUE
#| echo: TRUE
library(text2map)

dtm <- df_tng |> 
      dtm_builder(text, title, chunk = 10L) |> 
      dtm_stopper(stop_hapax = TRUE)



dim(dtm)

```

Note: We're removing terms that only occur once (hapax). This greatly reduced the computational complexity, and also these rare terms are unlikely to have accurate vector representations with these algorithms.

## Extended Inductive: Embeddings

- Built Term-Context Matrix (TCM)
  - How many times a word occurs in the same $n$-term window as another word
  - We really should "control" for how common a term is in the corpus as a whole

## Extended Inductive: Embeddings


- Built Term-Context Matrix (TCM)
  - How many times a word occurs in the same $n$-term window as another word
  - We really should "control" for how common a term is in the corpus as a whole
  - We will weight the TCM by "Positive Pointwise Mutual Information" (PPMI)


## Extended Inductive: Embeddings

- "Pointwise Mutual Information" (PMI)
    - probability of two words co-occurring
    - divided by each word occurring at all
    - then we get the log

## Extended Inductive: Embeddings

- **Positive**-Pointwise Mutual Information (PPMI)
  - PMI score could range from negative $\infty$ to positive $\infty$
  - But, negative numbers cause hassles, so we set all negatives to zero^[Negative numbers "imply things are co-occurring less often than we would expect by chance... [and] tend to be unreliable unless our corpora are enormous... and it's not clear whether it's even possible to evaluate such scores of 'unrelatedness' with human judgments" [@Jurafsky2021-ws]]

## Extended Inductive: Embeddings

Create a TCM function that weights by PPMI:
```{r}
#| eval: TRUE
#| echo: TRUE
tcm_builder <- function(mat) {
  
  ## projection
  tcm <- crossprod(mat)
  ## deal with self-occurrences 
  diag(tcm) <- diag(tcm) - (colSums(mat) - 1)
  ## weight by PMI
  tcm <- tcm %*% diag(1 / diag(tcm))
  tcm <- log(tcm)
  ## positive PMI
  tcm[tcm < 0 ] <- 0
  return(Matrix(tcm, sparse = TRUE))
  
}
```

## Extended Inductive: Embeddings

Use our function on our chunked DTM
```{r}
#| eval: TRUE
#| echo: TRUE
tcm_ppmi <- tcm_builder(dtm)

```

## Extended Inductive: Embeddings


- Cosine similarity
  - Common way to compare how *similar* two vectors are
  - Basically, if they go up and down together
  - Bigger numbers mean **more** similarity

## Extended Inductive: Embeddings {.smaller}

We'll use the cosine similarity function from `text2vec`

```{r}
#| eval: TRUE
#| echo: TRUE

library(text2vec)

```

Compare the similarity for vectors for specific words in our corpus:
```{r}
#| eval: TRUE
#| echo: TRUE

vec1 <- tcm_ppmi["captain", , drop = FALSE]
vec2 <- tcm_ppmi["picard", , drop = FALSE]
sim2(vec1, vec2, method = "cosine")
```
```{r}
#| eval: TRUE
#| echo: TRUE
vec1 <- tcm_ppmi["riker", , drop = FALSE]
vec2 <- tcm_ppmi["picard", , drop = FALSE]
sim2(vec1, vec2, method = "cosine")
```

## Extended Inductive: Embeddings {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE

focal <- c("boy", "girl")
vecs <- tcm_ppmi[focal, ]

sims <- sim2(vecs, tcm_ppmi, method = "cosine")

df_sims <- data.frame(word1 = sims[1, ],
                      word2 = sims[2, ],
                      term  = colnames(sims))

df_plot <- df_sims |> 
mutate(gender_bias = word1 - word2,
      gender = ifelse(gender_bias > 0, "boy", "girl"),
      gender_bias = abs(gender_bias)) 
```

## Extended Inductive: Embeddings {.smaller}

```{r}
#| eval: TRUE
#| echo: TRUE
#| output-location: slide
#| warning: FALSE
#| message: FALSE

df_plot |>
   group_by(gender) |> 
   slice_max(gender_bias, n = 30) |>
   mutate(term = fct_reorder(term, gender_bias)) |>
   ggplot(aes(term, gender_bias, fill = gender, label = term)) + 
   geom_col() +
   coord_flip() +
   facet_wrap(~gender, scale = "free")

```


## Extended Inductive: Embeddings

- In a TCM:
  - **Target words** (rows) are represented by their shared contexts with **context words** (columns)
    - But, maybe there are "latent" meanings that will better represent target words
    - Can we "reduce" the dimensions (i.e. columns)?

## Extended Inductive: Embeddings


- Singular Value Decomposition (SVD)!^[Again, see __Math Basics__ for a refresher.]
  - Exactly what we used for (LSA) topic modeling -- only we're starting with a different matrix


## Extended Inductive: Embeddings


- Singular Value Decomposition (SVD)!^[Again, see __Math Basics__ for a refresher.]
  - Exactly what we used for (LSA) topic modeling -- only we're starting with a different matrix
  - A type of *factorization*: 
      - break stuff down into smaller things which when multiplied together give you the original thing

## Extended Inductive: Embeddings

This will run SVD on our PPMI-weighted TCM. Don't run in class -- it will take around 10-20 minutes on this corpus!

```{r}
#| eval: FALSE
#| echo: TRUE
#| purl: TRUE
svd_tng <- readRDS("svd_tng.Rds")

```

```{r}
#| eval: FALSE
#| echo: TRUE
#| purl: TRUE
# don't run in class

# svd_tng <- svd(tcm_ppmi)
```

```{r}
#| eval: FALSE
#| echo: TRUE
#| purl: FALSE

svd_tng <- svd(tcm_ppmi)
```

```{r}
#| eval: FALSE
#| echo: FALSE
#| purl: FALSE

# 8:28
saveRDS(svd_tng, "data/svd_tng.Rds")

```
```{r}
#| eval: TRUE
#| echo: FALSE
#| purl: FALSE

svd_tng <- readRDS("../data/svd_tng.Rds")

```

## Extended Inductive: Embeddings

Let's explore the output:
```{r}
#| eval: TRUE
#| echo: TRUE
names(svd_tng)
```
```{r}
#| eval: TRUE
#| echo: TRUE
dim(svd_tng$u)
```

```{r}
#| eval: TRUE
#| echo: TRUE
length(svd_tng$d)
```

```{r}
#| eval: TRUE
#| echo: TRUE
dim(svd_tng$v)
```


## Extended Inductive: Embeddings

Lots of work shows somewhere between 100 and 300 dimensions are optimal for representing words:
```{r}
#| eval: TRUE
#| echo: TRUE

# let's get the first 100 dimensions
wv_tng <- svd_tng$v[, 1:100]
rownames(wv_tng) <- rownames(tcm_ppmi)

```

We now have **word embeddings**!


## Extended Inductive: Embeddings

Let's find the cosine between all words and a couple focal words:

```{r}
#| eval: TRUE
#| echo: TRUE


focal <- c("boy", "girl")

vecs <- wv_tng[focal, ]
sims <- sim2(vecs, wv_tng, method = "cosine")

df_sims <- data.frame(word1 = sims[1, ],
                      word2 = sims[2, ],
                      term  = colnames(sims))
```

## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE

df_plot <- df_sims |> 
mutate(gender_bias = word1 - word2,
      gender = ifelse(gender_bias > 0, "boy", "girl"),
      gender_bias = abs(gender_bias)) 
```

## Extended Inductive: Embeddings

```{r}
#| eval: TRUE
#| echo: TRUE
#| output-location: slide
#| warning: FALSE
#| message: FALSE

df_plot |>
   group_by(gender) |> 
   slice_max(gender_bias, n = 30, with_ties = FALSE) |>
   mutate(term = fct_reorder(term, gender_bias)) |>
   ggplot(aes(term, gender_bias, fill = gender, label = term)) + 
   geom_col() +
   coord_flip() +
   facet_wrap(~gender, scale = "free")

```


## Extended Inductive: Embeddings


- You just "trained" word embeddings on *Star Trek* scripts!
  - Are we picking up on something unique to *Star Trek*
  - Or, are these associations in our culture more broadly?
  - Or, are these associations in language use "in general"?


## Extended Inductive: Embeddings

- Embedding algorithms
  - There are many ways to create embeddings
    - Counting co-occurences: we could limit the max co-occurrences
    - Varing context windows: larger or smaller, or only left/right
    - Negative sampling: only update a few words at a time
    - Changing tokenizing: include common bigrams or even character-ngrams
  - They are all very similar to using SVD on a PPMI weighted TCM



## References {.allowframebreaks}